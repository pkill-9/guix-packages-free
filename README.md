This repository is a [guix channel](https://guix.gnu.org/manual/en/guix.html#Channels) that contains package definitions, service definitions, and other guix utilities, that I have created. I have aimed to keep package definitions for only packages that are freely licensed with the source available.

It contains packages and services that haven't been contributed to Guix upstream, either because I haven't gotten round to doing so yet, or because they won't be upstreamed for some reason - for example, the Openra and Scrcpy packages currently use prebuilt downloaded binaries which are free, but currently can't be built from source with Guix due to missing dependencies (in the case of scrcpy - it needs Android Studio) or that they won't seem to build (Openra uses a bunch of small .Net DLL files which fail to build with Mono).

# Warnings

This channel includes package definitions for some packages that can't be built from source (for example, discord-chat-exporter) that are merely wrapped binaries provided from other people/organisations that are not audited - Even though the source is available, the process by which they build the distributed executables can never truly be known. You trust these at your own risk.

You use this channel, and the software provided in it, at your own risk.

# Installation

Pkill9-guix-free can be installed as a
[Guix channel](https://www.gnu.org/software/guix/manual/en/html_node/Channels.html).
To do so, add it to ~/.config/guix/channels.scm:

```
  (cons* (channel
          (name 'pkill9-free)
          (url "https://gitlab.com/pkill-9/guix-packages-free"))
         %default-channels)
```

# How to use FHS binaries compatibility service in your Guix configuration

See [my system configuration](https://gitlab.com/pkill-9/guix-config/blob/72d14eb71b665a4e228783c49abae8fe1c74d4f0/desktop.scm#L193) as an example.

Once your system has been reconfigured with this service added, you can run portable linux binaries, like Tar bundles and Appimages (All the ones I've tested have run so far).

```
                        _
                       | \
                       | |
                       | |
  |\                   | |
 /, ~\                / /
X     `-.....-------./ /
 ~-. ~  ~              |
    \             /    |
     \  /_     ___\   /
     | /\ ~~~~~   \ |
     | | \        || |
     | |\ \       || )
    (_/ (_/      ((_/
```
