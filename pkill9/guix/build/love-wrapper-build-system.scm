;;Todo: Check the love and bash inputs are not native-inputs, i.e. won't get colected by the garbage collector
;; Put gzip in PATH for tar
;; Fix this warning: ;;; /gnu/store/dm8jzqwznxv10qs2z9qhrl00qf1g6sgg-module-import/pkill9/guix/build/love-wrapper-build-system.scm:96:10: warning: "#!~a/bin/bash~@\\n~@\\n~a/bin/love ~a~%": unsupported format option ~@, use (ice-9 format) instead

(define-module (pkill9 guix build love-wrapper-build-system)
  #:use-module ((guix build gnu-build-system) #:prefix gnu:)
  #:use-module (guix build utils)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 ftw)
  #:export (%standard-phases
            love-wrapper-build))

;; Commentary:
;;
;; Builder-side code of the build procedure for LÖVE wrapper packages.
;;
;; Code:

(define (extract-name-from-name-var name)
  (let ((regex-match (string-match "(.*)-.*$" name)))
    (if regex-match
        (match:substring regex-match 1)
        (error "Failed to extract actual name from name variable"))))

(define (move-first-love-file directory destination)
  (let ((love-files (find-files
                     directory
                     (lambda (file stat)
                       (string-suffix? ".love" file)))))
    (if (= 0 (length love-files))
        (error "No LOVE files found in this directory.")
        (rename-file
         (first love-files)
         destination))))

(define* (unpack #:key name source inputs outputs #:allow-other-keys)
  (let* ((out (assoc-ref outputs "out"))
         (unzip (string-append (assoc-ref inputs "unzip") "/bin/unzip"))
         (tar (string-append (assoc-ref inputs "tar") "/bin/tar"))
         (zip (string-append (assoc-ref inputs "zip") "/bin/zip"))
         (name (extract-name-from-name-var name)))
    (begin
      (cond ((eq? (stat:type (stat source)) 'directory)
             (begin
               (copy-recursively source "source")
               (chdir "source")
               (invoke zip "pack.love"
                       "-r" ".")
               (chdir "..")
               (rename-file
                "source/pack.love"
                "love-file.love")))
            ((string-suffix? ".zip" source)
             (begin
               (mkdir "archive")
               (invoke unzip "-d" "archive" source)
               (move-first-love-file "archive" "love-file.love")))
            ((or (string-suffix? ".tar" source)
                 (string-suffix? ".tar.gz" source)
                 (string-suffix? ".tar.xz" source)
                 )
             (begin
               (setenv "PATH" (string-append (assoc-ref inputs "xz") "/bin"))
               (mkdir "archive")
               (invoke tar "xvf" source "-C" "archive")
               (display "I'm assuming this is the source code.")
               ;;FIXME: this is ugly
               (chdir (string-append "archive/" (list-ref (scandir "archive") 2)))
               (display (getcwd))
               (invoke zip "pack.love"
                       "-r" ".")
               (chdir "../..")
               (move-first-love-file (string-append "archive/" (list-ref (scandir "archive") 2)) "love-file.love")
               ))
            ((string-suffix? ".love" source)
             (copy-file source "love-file.love"))
            (else
             (begin
               (display "I'm assuming this is the source code.")
               (invoke zip "pack.love"
                       "-r" "." "-i" ".")
               (chdir "..")
               (rename-file
                "source/pack.love"
                "love-file.love")))))
    #t))

(define* (build #:key name category source inputs outputs #:allow-other-keys)
  "Copy SOURCE into the build directory."
  (let* ((out (assoc-ref outputs "out"))
         (bash (assoc-ref inputs "bash"))
         (love (assoc-ref inputs "love"))
         (icon (assoc-ref inputs "icon"))
         (package-name (extract-name-from-name-var name))
         (bin-name package-name)
         (title-name (string-capitalize
                      (string-map (lambda (c)
                                    (if (char=? #\- c) #\space c))
                                  package-name)))
         (love-files-dir "share/love-files")
         (love-relative-file-path (string-append love-files-dir "/"
                                                 (string-append package-name ".love"))))

    (begin
      ;; Prepare build directory
      (mkdir "build")
      (chdir "build")
      (mkdir "bin")
      (mkdir-p love-files-dir)
      (mkdir-p "share/applications")

      ;; Move LOVE file into build directory
      (rename-file "../love-file.love" love-relative-file-path)

      ;; Generate and write a script to run the LOVE file
      (with-output-to-file (string-append "bin/" bin-name)
        (lambda _
          (format #t
                  "#!~a/bin/bash~@
~@
~a/bin/love ~a~%" bash love (string-append
                             out "/" love-relative-file-path))))
      (chmod (string-append "bin/" bin-name) #o755)

      ;; Generate and write a Desktop file for desktop environments
      (with-output-to-file (string-append "share/applications/" bin-name ".desktop")
        (lambda _
          (format #t
                  "[Desktop Entry]~@
                     Name=~a~@
                     Comment=A game made with LOVE.~@
                     Exec=~a~@
                     TryExec=~a~@
                     Icon=~a~@
                     Categories=~a~@
                     Type=Application~%"
                  title-name
                  (string-append out "/bin/" bin-name)
                  (string-append out "/bin/" bin-name)
                  icon
                  category)))
      #t)))

(define* (install #:key inputs outputs #:allow-other-keys)
  "Install the package contents."
  (let* ((out (assoc-ref outputs "out"))
         (source (getcwd)))
    (copy-recursively source out)
    #t))

(define %standard-phases
  ;; Standard build phases, as a list of symbol/procedure pairs.
  (let-syntax ((phases (syntax-rules ()
                         ((_ p ...) `((p . ,p) ...)))))
    (phases unpack build install)))

(define* (love-wrapper-build #:key inputs (phases %standard-phases)
                             #:allow-other-keys #:rest args)
  "Build the given love-wrapper package, applying all of PHASES in order."
  (apply gnu:gnu-build #:inputs inputs #:phases phases args))
