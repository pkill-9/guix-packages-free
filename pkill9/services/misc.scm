;; Maybe use separate caches for different architectures of glibc, so that binaries don't look for incorrect architectures? Then again I don't think it's a problem.
;; Move generation of package union to outside packages->ld.so.conf and make that function able to generate the conf file using any list of packages/store-paths.
;; Use configuration record for service that includes glibc files, additional packages to put in system profile (for paths and share data), and arbitrary additional paths, all with sane defaults.
;; WARNING: special-files-service doesn't raise an error ("" ,(@ (gnu packages ) ))f given invalid arguemnt, e.g. a "#f" or an empty list - the operating system will error on boot and drop into a guile shell when it fails to be able to use that argument to place special files.
;; Electron apps are failing due to missing fonts, and fail catastrophically and dont even give an error message other than "breakpoint trap"
(define-module (pkill9 services misc)
  #:use-module (ice-9 ftw) ;; for creating recursive list of directories of libs for FHS  #:use-module (guix download)
  #:use-module (srfi srfi-1) ;; For "remove" for delete-service
  #:use-module (guix records) ;; For defining record types
  #:use-module (guix profiles) ;; for specifications->manifest and manifest-entries
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (guix gexp)
  #:use-module (guix build-system trivial)
  #:use-module (guix packages)
  #:use-module (gnu system accounts) ;; For 'user-account'
  #:use-module (gnu system shadow) ;; For 'account-service-type'
  #:use-module (gnu packages)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages base))

(define-public (delete-service service-type services)
  (remove
   (lambda (service)
     (eq? (service-kind service) service-type))
   services))

  (define-public %brightness-service
    ;; Run as a Shepherd service instead of an activation script since the
    ;; latter typically runs before all modules have been loaded.
    (simple-service 'brightness-service shepherd-root-service-type
                    (list (shepherd-service
                           (provision '(bash))
                           (requirement '())
                           (start #~(lambda ()
                                      (invoke
                                       #$(file-append bash "/bin/bash")
                                       "-c"
                                       "echo 50 > /sys/class/backlight/intel_backlight/brightness")))
                           (respawn? #f)))))

  (define-public %powertop-service
    ;; Run as a Shepherd service instead of an activation script since the
    ;; latter typically runs before all modules have been loaded.
    (simple-service 'powertop shepherd-root-service-type
                    (list (shepherd-service
                           (provision '(powertop))
                           (requirement '())
                           (start #~(lambda ()
                                      (invoke
                                       #$(file-append powertop "/sbin/powertop")
                                       "--auto-tune")))
                           (respawn? #f)))))

;;fhs-binaries-compatibility-service-type

;;(fhs-libs-union (map (lambda (pkg) (specification->package+output pkg)) fhs-packagelist))
;;(fhs-libs-union fhs-package-variables-list)
;;(fhs-linker-config-bundle fhs-package-variables-list)
