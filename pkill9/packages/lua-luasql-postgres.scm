(define-module (pkill9 packages lua-luasql-postgres)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages databases))

(define-public lua5.1-luasql-postgres
  (package
    (name "lua5.1-luasql-postgres")
    (version "2.5.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/keplerproject/luasql")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1jdm1abj2ngklg7syq1ijj142ai9nmdl9370dk2bgamzlxc41pqm"))))
    (build-system gnu-build-system)
    (inputs
     `(("lua" ,lua-5.1)
       ("postgresql" ,postgresql)))
    (arguments
     `(#:tests? #f ;; No tests
       #:make-flags (let ((out (assoc-ref %outputs "out")))
                      (list
                       (string-append "LUA_LIBDIR=" out "/lib/lua/" "5.1")))
       #:phases (modify-phases %standard-phases
                  (delete 'configure) ;; No configure target
                  (replace 'build
                    (lambda* (#:key inputs #:allow-other-keys)
                      (let ((make (string-append (assoc-ref inputs "make") "/bin/make")))
                        (invoke make "postgres")
                        #t))))))
    (home-page "http://keplerproject.github.io/luasql")
    (synopsis "Interface from Lua to Postgres")
    (description "synopsis")
(license license:x11)))

lua5.1-luasql-postgres
