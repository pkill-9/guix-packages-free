(define-module (pkill9 packages homebank)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses)
 )

; Example arguments to use:
;    (arguments
;     `(#:tests? #f
;       #:make-flags '("CC=gcc" "RM=rm" "SHELL=sh" "all")
;       #:phases (modify-phases %standard-phases (delete 'configure))
;       ))

(define-public homebank
(package
  (name "homebank")
  (version "5.2.2")
  (source
    (origin
      (method url-fetch)
      (uri (string-append
             "http://homebank.free.fr/public/homebank-"
             version
             ".tar.gz"))
      (sha256
        (base32
          "19cm49p2x6nwia2yvwj3fv7jxbhw0vx4bs1zqbfvdr5vzwgj5j5c"))))
  (build-system gnu-build-system)
  (native-inputs
    `(("pkg-config"
       ,(@ (gnu packages pkg-config) %pkg-config))
      ("intltool" ,(@ (gnu packages glib) intltool))))
  (inputs
    `(("gtk+" ,(@ (gnu packages gtk) gtk+))
      ("glib" ,(@ (gnu packages glib) glib))
      ("fribidi" ,(@ (gnu packages fribidi) fribidi))
      ("libsoup" ,(@ (gnu packages gnome) libsoup))))
  (home-page "")
  (synopsis "")
  (description "")
  (license gpl3+))

 )
(package (inherit homebank))
