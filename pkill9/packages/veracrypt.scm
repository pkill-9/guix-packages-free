;; Thanks to terpri in freenode.#guix for providing the install phase patch.
;; Need to modify makefiles such that they don't place files under 'usr' and change the .desktop file as it uses the line 'Exec=/usr/bin/veracrypt'.
;; Also need to make replacing the instlal phase unnecessary.

(define-module (pkill9 packages veracrypt)
  #:use-module (gnu packages)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages linux)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses))

(define-public veracrypt
  (package
   (name "veracrypt")
   (version "1.21")
   (source
    (origin
     (method url-fetch)
     (uri (string-append
           "https://launchpad.net"
           "/veracrypt/trunk/" version "/+download/"
           "VeraCrypt_" version "_Source.tar.bz2"))
     (sha256
      (base32
       "0n036znmwnv70wy8r2j3b55bx2z3cch5fr83vnwjvzyyp0j7swa4"))))
   (native-inputs
    `(("grep" ,(@ (gnu packages base) grep))
      ("sed" ,(@ (gnu packages base) sed))
      ("pkg-config"
       ,(@ (gnu packages pkg-config) %pkg-config))
      ("yasm" ,(@ (gnu packages assembly) yasm))))
   (inputs
    `(("wxwidgets"
       ,(@ (gnu packages wxwidgets) wxwidgets))
      ("bash" ,bash)
      ("util-linux" ,util-linux)
      ("fuse" ,(@ (gnu packages linux) fuse)))) ; fuse may be able to just be a propagated input
   (propagated-inputs
    `(("lvm2" ,(@ (gnu packages linux) lvm2)))) ; For dmsetup
   (build-system gnu-build-system)
   (arguments
    `(#:tests? #f ;; no test target in Makefile, but there is a "Tests" directory
      #:make-flags (list "CC=gcc"
                         (string-append "DESTDIR=" (assoc-ref %outputs "out"))
                         (string-append "PWD=" (getcwd)"/src")) ;; relies on patch phase
      #:phases (modify-phases %standard-phases
                              (delete 'configure)
                              (add-after 'unpack 'patch-makefile ;; making it a snippet seems to break the source output, it only produces 'doc' directory
                               (lambda _
                                 (substitute* "../src/Main/Main.make"
                                              (("cp -R \\$\\(CURDIR\\)") "cp -R $(PWD)"))))
                              (add-before 'build 'change-to-source-directory
                                          (lambda _
                                            (chdir "../src")
                                            #t))
                              (add-before 'install 'make-output-dir ;; no idea why this is needed tbh
                                       (lambda* (#:key outputs #:allow-other-keys)
                                         (let ((out (assoc-ref %outputs "out")))
                                           (mkdir out)
                                           #t)))
                              (add-after 'install 'wrap-executable-for-dmsetup
                                         (lambda* (#:key inputs outputs #:allow-other-keys)
                                           (let ((out (assoc-ref outputs "out"))
                                                 (lvm2-binpath (string-append (assoc-ref inputs "lvm2") "/sbin"))
                                                 (util-linux-binpath (string-append (assoc-ref inputs "util-linux") "/sbin")))
                                             (wrap-program (string-append out "/usr/bin/veracrypt")
                                                           `("PATH" ":" prefix (,lvm2-binpath "/run/setuid-programs" ,util-linux-binpath)))
                                             #t)))
                              (add-after 'install 'add-bin-symlink
                               (lambda* (#:key outputs #:allow-other-keys)
                                 (let ((out (assoc-ref %outputs "out")))
                                   (mkdir (string-append out "/bin"))
                                   (symlink (string-append out "/usr/bin/veracrypt")
                                            (string-append out "/bin/veracrypt"))
                                   #t))))))
   (home-page "https://www.veracrypt.fr")
   (synopsis "VeraCrypt is a free open source disk encryption software.")
   (description "VeraCrypt adds enhanced security to the algorithms used for
system and partitions encryption making it immune to new developments in
brute-force attacks.  VeraCrypt also solves many vulnerabilities and
security issues found in TrueCrypt.")
   (license asl2.0)))

veracrypt
