(define-module (pkill9 packages unvanquished)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system trivial)
  #:use-module (guix licenses)

  #:use-module (gnu packages base) ;for tar
  #:use-module (gnu packages compression) ;for bzip2
 )

; Example arguments to use:
;    (arguments
;     `(#:tests? #f
;       #:make-flags '("CC=gcc" "RM=rm" "SHELL=sh" "all")
;       #:phases (modify-phases %standard-phases (delete 'configure))
;       ))

(define unvanquished/external-deps
  (package
    (name "unvanquished-external-deps")
    (version "0")
    (source
     (origin
       (method url-fetch)
       (uri "http://dl.unvanquished.net/deps/linux64-5.tar.bz2")
       (sha256
        (base32
         "05zbhw91dxaz51cpq7l7x21qrd8hrhh45dhgd2s0yswj0f2abi8d"))))
    (build-system trivial-build-system)
    (native-inputs
     `(("tar" ,tar)
       ("bzip2" ,bzip2)))
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let* ((out (assoc-ref %outputs "out"))
                (source (assoc-ref %build-inputs "source"))
                (tar (string-append (assoc-ref %build-inputs "tar") "/bin/tar")))
	   (setenv "PATH" (string-append (assoc-ref %build-inputs "bzip2") "/bin")) ; so tar finds bzip2 to use for extraction
           (invoke tar "xvf" source)
           (mkdir-p out)
           (copy-recursively "linux64-5"
                             (string-append out "/linux64-5"))))))
    (home-page "")
    (synopsis "External dependencies for Unvanquished")
    (description
     "Dependencies downloaded during the build process for Unvanquished, seems to be for pnacl.")
    (license #f)))

(define unvanquished/data
  (package
    (name "unvanquished-data") ;;in future, build the assets from source as README explains how to
    (version "0")
    (source
     (origin
       (method url-fetch)
       (uri "https://github.com/Unvanquished/Unvanquished/releases/download/v0.52.0/unvanquished_0.52.0.zip")
       (sha256
        (base32
         "1gpn0q0ysslx9y6g93b0m2icfgi6pwif3gg07g4jlk3g883azw9a"))))
    (build-system trivial-build-system)
    (native-inputs
     `(("unzip" ,unzip)))
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let* ((out (assoc-ref %outputs "out"))
                (source (assoc-ref %build-inputs "source"))
                (unzip (string-append (assoc-ref %build-inputs "unzip") "/bin/unzip")))
           (invoke unzip source)
           (mkdir-p (string-append out "/share"))
           (copy-recursively "unvanquished_0.52.0/pkg"
                             (string-append out "/share/unvanquished"))))))
    (home-page "")
    (synopsis "External dependencies for Unvanquished")
    (description
     "Dependencies downloaded during the build process for Unvanquished, seems to be for pnacl.")
    (license #f)))

(define-public unvanquished
(package
  (name "unvanquished")
  (version "0.52.0")
  (source
    (origin
      (method git-fetch)
      (uri (git-reference
               (url "https://github.com/Unvanquished/Unvanquished.git")
	       (commit (string-append "v" version))
	       (recursive? #t)))
      (sha256
         (base32
           "1acda1559q6zwmhg3x00nai88hy83i5hcfli2bqfab7slr95lm27"))))
  (build-system cmake-build-system)
  (native-inputs
    `(("python2" ,(@ (gnu packages python) python-2.7))
      ("python2-pyyaml"
       ,(@ (gnu packages python-xyz) python2-pyyaml))
      ("python2-jinja2"
       ,(@ (gnu packages python-xyz) python2-jinja2))
      ("unvanquished/external-deps"
         ,unvanquished/external-deps)))
  (inputs
    `(("zlib" ,(@ (gnu packages compression) zlib))
      ("gmp" ,(@ (gnu packages multiprecision) gmp))
      ("nettle" ,(@ (gnu packages nettle) nettle))
      ("curl" ,(@ (gnu packages curl) curl))
      ("sdl2" ,(@ (gnu packages sdl) sdl2))
      ("glew" ,(@ (gnu packages gl) glew))
      ("libpng" ,(@ (gnu packages image) libpng))
      ("libjpeg" ,(@ (gnu packages image) libjpeg))
      ("libwebp" ,(@ (gnu packages image) libwebp))
      ("freetype"
       ,(@ (gnu packages fontutils) freetype))
      ("openal" ,(@ (gnu packages audio) openal))
      ("libogg" ,(@ (gnu packages xiph) libogg))
      ("libvorbis" ,(@ (gnu packages xiph) libvorbis))
      ("libtheora" ,(@ (gnu packages xiph) libtheora))
      ("opus" ,(@ (gnu packages xiph) opus))
      ("opusfile" ,(@ (gnu packages xiph) opusfile))
      ("lua" ,(@ (gnu packages lua) lua))
      ("unvanquished-data" ,unvanquished/data)))
  (arguments
   `(#:tests? #f
     #:configure-flags (list (string-append "-DCMAKE_CXX_FLAGS=-I" (assoc-ref %build-inputs "opus") "/include/opus") "-DUSE_CURSES=OFF" "-DUSE_GEOIP=OFF" "-DBUILD_GAME_NACL=0" "-DBUILD_GAME_NACL_NEXE=0" (string-append "-DCMAKE_INSTALL_PREFIX=" (assoc-ref %outputs "out") "/opt/unvanquished")) ;opusfile header file is miswritten
     #:phases (modify-phases %standard-phases
			     (add-before 'configure 'add-extra-deps
					 (lambda _
					   (symlink
					     (string-append (assoc-ref %build-inputs "unvanquished/external-deps") "/linux64-5")
					     "daemon/external_deps/linux64-5")
					   #t))
			     (replace 'install
				      (lambda _
					(copy-recursively "."
							  (string-append (assoc-ref %outputs "out") "/opt/unvanquished"))
					#t))
			     (add-after 'install 'add-bin-link
					(lambda _
					  (mkdir (string-append (assoc-ref %outputs "out") "/bin"))
					  (symlink (string-append (assoc-ref %outputs "out") "/opt/unvanquished/daemon")
						   (string-append (assoc-ref %outputs "out") "/bin/unvanquished"))
					  #t))
			     (add-after 'add-bin-link 'wrap-main-executable
					(lambda _
					  (wrap-program (string-append (assoc-ref %outputs "out") "/bin/unvanquished"))
					  (substitute* (string-append (assoc-ref %outputs "out") "/bin/unvanquished")
						       (("\\$@") (string-append "-set\" \"vm.cgame.type\" \"3\" \"-set\" \"vm.sgame.type\" \"3\" \"-pakpath\" \"" (assoc-ref %build-inputs "unvanquished-data") "/share/unvanquished" "\" \"$@")))
					  #t))
			      )))
  (home-page "")
  (synopsis "")
  (description "")
  (license gpl3+))

 )
(package (inherit unvanquished))
