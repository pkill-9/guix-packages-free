(define-module (pkill9 packages scrcpy)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system meson)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages))

(define scrcpy-server
  (let* ((version "1.8"))
    (origin
     (method url-fetch)
     (uri (string-append "https://github.com/Genymobile/scrcpy"
                         "/releases/download/v" version "/"
                         "scrcpy-server-v" version ".jar"))
     (sha256
      (base32
       "1h755k5xpchlm7wq2yk5mlwjnh7y4yhviffixacby0srj3pmb443")))))

(define-public scrcpy
  (package
   (name "scrcpy")
   (version "1.8")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/Genymobile/scrcpy")
                  (commit (string-append "v" version))))
            (file-name (git-file-name name version))
            (sha256
             (base32
              "1cx7y3w699s3i8s53l1mb7lkrnbix457hf17liwh00jzb0i7aga7"))))
   (build-system meson-build-system)
   (native-inputs
    `(("pkg-config" ,(@ (gnu packages pkg-config) pkg-config))))
   (inputs
    `(("ffmpeg" ,(@ (gnu packages video) ffmpeg))
      ("sdl2" ,(@ (gnu packages sdl) sdl2))
      ("scrcpy-server" ,scrcpy-server)
      ("adb" ,(@ (gnu packages android) adb))))
   (arguments
    `(#:configure-flags (list (string-append "-Dprebuilt_server="
                                             (assoc-ref %build-inputs "scrcpy-server")))
      #:phases (modify-phases %standard-phases
                              (add-after 'install 'wrap-executable-with-adb-path
                                         (lambda* (#:key inputs outputs #:allow-other-keys)
                                           (let* ((out (assoc-ref outputs "out"))
                                                  (adb (assoc-ref inputs "adb")))
                                             (wrap-program (string-append out "/bin/scrcpy")
                                                           `("PATH" ":" prefix (,(string-append adb "/bin/adb"))))))))))
   (home-page "https://github.com/Genymobile/scrcpy")
   (synopsis "Display and control your Android device")
   (description "Scrcpy (screen copy) provides display and control of Android devices connected on USB (or over TCP/IP). It does not require any root access.")
   (license license:asl2.0)))

scrcpy
