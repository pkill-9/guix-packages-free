(define-module (pkill9 packages jgmenu)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses)
 )

; Example arguments to use:
;    (arguments
;     `(#:tests? #f
;       #:make-flags '("CC=gcc" "RM=rm" "SHELL=sh" "all")
;       #:phases (modify-phases %standard-phases (delete 'configure))
;       ))

(define-public jgmenu
(package
  (name "jgmenu")
  (version "0.9.1")
  (source
    (origin
      (method url-fetch)
      (uri (string-append
             "https://github.com/johanmalm/jgmenu/archive/v"
             version
             ".tar.gz"))
      (sha256
        (base32
          "067dzcvdisdl3s4h1y9shz2vbhb5flmih2zq95zq483azx0kvpaq"))))
  (build-system gnu-build-system)
  (native-inputs
    `(("pkg-config" ,(@ (gnu packages pkg-config) pkg-config))
      ("perl" ,(@ (gnu packages perl) perl)) ; used for tests. Although the tests are broken because the package build succeeds even when the tests don't find the perl binary.
      ))
  (inputs
    `(("libxml2" ,(@ (gnu packages xml) libxml2))
      ("cairo" ,(@ (gnu packages gtk) cairo))
      ("pango" ,(@ (gnu packages gtk) pango))
      ("libxinerama" ,(@ (gnu packages xorg) libxinerama))
      ("librsvg" ,(@ (gnu packages gnome) librsvg))
      ("python" ,(@ (gnu packages python) python-3)) ; some bundled scripts want this
      ))
  (arguments
    `(#:make-flags (list "CC=gcc" (string-append "prefix=" (assoc-ref %outputs "out")))
      #:phases (modify-phases %standard-phases (delete 'configure))))
  (home-page "")
  (synopsis "")
  (description "")
  (license gpl3+))

 )
(package (inherit jgmenu))
