(define-module (pkill9 packages orca-c)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages music))

(define-public orca-c
  (package
   (name "orca-c")
   (version "git")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/hundredrabbits/Orca-c")
                  (commit "d7a3b169c5ed0b06a9ad0fdb3057704da9a0b6ce")))
            (file-name (git-file-name name version))
            (sha256
             (base32
              "101y617a295hzwr98ykvza1sycxlk29kzxn2ybjwc718r0alkbzz"))))
   (build-system gnu-build-system)
   (arguments
    `(#:make-flags '("CC=gcc")
      #:tests? #f ;; No tests.
      #:phases (modify-phases %standard-phases
                              (delete 'configure) ;; No configure phase.
                              (replace 'install
                                       (lambda* (#:key outputs #:allow-other-keys)
                                         (let ((out (assoc-ref outputs "out")))
                                           (install-file "build/orca"
                                                         (string-append out "/bin"))
                                           (install-file "README.md"
                                                         (string-append out "/share/doc/orca-c-git"))
                                           (copy-recursively "examples"
                                                         (string-append out "/share/orca-c-git/examples"))
                                           #t))))))
   (inputs
    `(("ncurses" ,ncurses)
      ("portmidi" ,portmidi)))
   (home-page "http://wiki.xxiivv.com/orca")
   (synopsis "Live programming environment (C port)")
   (description "Orca is an esoteric programming language designed to quickly create procedural sequencers, in which every letter of the alphabet is an operation, where lowercase letters operate on bang, uppercase letters operate each frame.

This application is not a synthesizer, but a flexible livecoding environment capable of sending MIDI, OSC & UDP to your audio/visual interfaces, like Ableton, Renoise, VCV Rack or SuperCollider.

This is the C implementation of the ORCΛ language and tools.  The livecoding environment for this C version runs in a terminal.  It's designed to be power efficient.  It can handle large files, even if your terminal is small.")
   (license license:expat)))

orca-c
