(define-module (pkill9 packages plank bamf)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses)
 )

; Example arguments to use:
;    (arguments
;     `(#:tests? #f
;       #:make-flags '("CC=gcc" "RM=rm" "SHELL=sh" "all")
;       #:phases (modify-phases %standard-phases (delete 'configure))
;       ))

(define-public bamf
(package
  (name "bamf")
  (version "0.5.3")
  (source
    (origin
      (method url-fetch)
      (uri (string-append
             "https://launchpad.net/bamf/0.5/"
             version
             "/+download/bamf-"
             version
             ".tar.gz"))
      (sha256
        (base32
          "051vib8ndp09ph5bfwkgmzda94varzjafwxf6lqx7z1s8rd7n39l"))
      (patches (search-patches "bamf.patch"))
      ))
  (build-system gnu-build-system)
  (arguments
   `(#:tests? #f; We're removing dependency checks for dependencies required only for the tests since they're not packaged in Guix.
     #:configure-flags (list (string-append "--prefix=" (assoc-ref %outputs "out"))) ;Prefix may not be needed cos it's appearing twice in the build output for configure flags.
     #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'fix-introspection-install-dir
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (substitute* '("configure")
                              ;"libs/pls/Makefile.in"
                              ;"libs/net/Makefile.in")
                 (("INTROSPECTION_GIRDIR=.*")
                  (string-append "INTROSPECTION_GIRDIR=" out "/share/gir-1.0/" "\n"))
                 (("INTROSPECTION_TYPELIBDIR=.*")
                  (string-append "INTROSPECTION_TYPELIBDIR=" out "/lib/girepository-1.0/" "\n"))))))
	 (add-after 'install 'link-to-bamfdaemon
		    ; put bamfdaemon in $PATH for vala-panel-appmenu build
           (lambda _
	     (mkdir-p (string-append (assoc-ref %outputs "out") "/bin"))
	     (symlink "../libexec/bamf/bamfdaemon" (string-append (assoc-ref %outputs "out") "/bin/bamfdaemon")))))))
  (native-inputs
    `(("pkg-config"
       ,(@ (gnu packages pkg-config) %pkg-config))
      ("libtool"
       ,(@ (gnu packages autotools) libtool)))) ; i noticed it's own libtool is running. putting it as input isn't working hmm
  (inputs
    `(("glib" ,(@ (gnu packages glib) glib))
      ("glib:bin" ,(@ (gnu packages glib) glib) "bin")
      ("libwnck" ,(@ (gnu packages gnome) libwnck))
      ("vala" ,(@ (gnu packages gnome) vala)) ; add bindings for vala api (may only need gio)
      ("gobject-introspection" ,(@ (gnu packages glib) gobject-introspection)) ; add bindings for vala api
      ("libgtop" ,(@ (gnu packages gnome) libgtop))))
  (home-page "")
  (synopsis "")
  (description "")
  (license gpl3+))
)
