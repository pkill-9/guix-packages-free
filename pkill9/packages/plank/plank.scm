(define-module (pkill9 packages plank plank)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses)
 )

; Example arguments to use:
;    (arguments
;     `(#:tests? #f
;       #:make-flags '("CC=gcc" "RM=rm" "SHELL=sh" "all")
;       #:phases (modify-phases %standard-phases (delete 'configure))
;       ))

(define-public plank
(package
  (name "plank")
  (version "0.11.4")
  (source
    (origin
      (method url-fetch)
      (uri (string-append
             "https://launchpad.net/plank/1.0/"
             version
             "/+download/plank-"
             version
             ".tar.xz"))
      (sha256
        (base32
          "1f41i45xpqhjxql9nl4a1sz30s0j46aqdhbwbvgrawz6himcvdc8"))))
  (build-system gnu-build-system)
  (arguments
   `(#:tests? #f)) ;; No access to X display server
  (native-inputs
    `(("pkg-config"
       ,(@ (gnu packages pkg-config) %pkg-config))
      ("bzip2" ,(@ (gnu packages compression) bzip2))
      ("intltool" ,(@ (gnu packages glib) intltool))))
  (inputs
    `(("glib:bin" ,(@ (gnu packages glib) glib) "bin")
      ("vala" ,(@ (gnu packages gnome) vala))
      ("libxml2" ,(@ (gnu packages xml) libxml2))
      ("gtk+" ,(@ (gnu packages gtk) gtk+))
      ("gdk-pixbuf" ,(@ (gnu packages gtk) gdk-pixbuf))
      ("cairo" ,(@ (gnu packages gtk) cairo))
      ("libgee" ,(@ (gnu packages gnome) libgee))
      ("libwnck" ,(@ (gnu packages gnome) libwnck))
      ("bamf" ,(@ (pkill9 packages plank bamf) bamf))
      ("libx11" ,(@ (gnu packages xorg) libx11))
      ))
  (home-page "")
  (synopsis "")
  (description "")
  (license gpl3+))
)

(define-public plank-git
  (package (inherit plank)
    (version "git")
    (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://git.launchpad.net/plank")
                      (commit "d00856dc966914c6d17cca47388959e153fbb733")))
                (sha256
                 (base32 "1fzscrapakr8gvcvgqqpirdyr84r2gm9zn88yrl4fqvybjbimpy4"))))
    ))
