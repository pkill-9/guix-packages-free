(define-module (pkill9 packages guix-module-creator)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (guix build-system python)
  #:use-module (guix licenses)
 )

(define-public guix-module-creator
(package
  (name "guix-module-creator")
  (version "0.0.1")
  (source
    (origin
      (method git-fetch)
	(uri (git-reference
	       (url "https://gitlab.com/pkill-9/guix-module-creator")
	       (commit "133a3e724c610e7e9693f2368b1aae230a2f57a7")))
      (sha256
        (base32
          "0nqzby8q2pv1f6xgkvsij5510q1v8mwv54h7gkljbimplbc26036"))))
  (build-system python-build-system)
  (home-page "")
  (synopsis "")
  (description "")
  (license gpl3+))

 )
(package (inherit guix-module-creator))
