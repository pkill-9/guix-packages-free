(define-module (pkill9 packages visidata)
  #:use-module (guix download)
  #:use-module (guix build-system python)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages time))

(define-public visidata
  (package
    (name "visidata")
    (version "1.5.2")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "visidata" version))
        (sha256
          (base32
            "10adfyn4gkisvciqawgh2lakkhhnjjxiyp7mzbgcwkq1b3sigpf1"))))
    (build-system python-build-system)
    (propagated-inputs
     `(("python-dateutil" ,python-dateutil)))
    (arguments
     `(#:tests? #f)) ;; tests broken
    (home-page "https://visidata.org")
    (synopsis
      "curses interface for exploring and arranging tabular data")
    (description
      "curses interface for exploring and arranging tabular data")
    (license license:gpl3)))

visidata
