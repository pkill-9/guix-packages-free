(define-module (pkill9 packages sc-im)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages maths))

(define-public sc-im
  (package
    (name "sc-im")
    (version "git")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/andmarti1424/sc-im")
               (recursive? #t)
               (commit "bdd936a12c68f74419e3307760439335e1b18133")))
        (file-name (git-file-name name version))
        (sha256
          (base32 "07gp25m3pjpsm7c8yxsd30q9p6rmpnb4i8b3c7m2g57g7nwb084q"))))
    (build-system gnu-build-system)
    (native-inputs
     `(("bison" ,bison)
       ("pkg-config" ,pkg-config)
       ("which" ,which)))
    (inputs
      `(("ncurses" ,ncurses)
        ("libxml2" ,libxml2)
        ("libzip" ,libzip)
        ("lua" ,lua)
        ("gnuplot" ,gnuplot)))
    (arguments
     `(#:tests? #f ;; No tests.
       #:make-flags `("CC=gcc"
                      ,(string-append "prefix=" (assoc-ref %outputs "out")))
       #:phases (modify-phases %standard-phases
                               (delete 'configure) ;; No configure file.
                               (add-before 'build 'chdir
                                           (lambda _
                                             (chdir "src"))))))
    (home-page "https://github.com/andmarti1424/sc-im")
    (synopsis "Terminal spreadsheet program")
    (description
      "SC-IM is an ncurses spreadsheet program that is based on SC.")
    (license "Custom")))

sc-im
