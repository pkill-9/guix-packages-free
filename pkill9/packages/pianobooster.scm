(define-module (pkill9 packages pianobooster)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages qt))

(define-public pianobooster
  (package
    (name "pianobooster")
    (version "git")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/captnfab/PianoBooster")
               (commit "acaa3a27dec7665b6fd54793f165a05b2d09a265")))
        (snippet
         '(begin
            (use-modules (guix build utils))
            (delete-file-recursively "src/3rdparty")))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1xb416gn162fr2w6kazqjcci39rsvw0dfll789bhjg4p57dxk7h9"))))
    (build-system cmake-build-system)
    (native-inputs ; It checks for the existence of jack, alsa and glu, for rtmidi.
      `(("pkg-config" ,pkg-config)
        ("jack" ,jack-1)
        ("alsa-lib" ,alsa-lib)
        ("glu" ,glu)))
    (inputs
      `(("qtbase" ,qtbase)
        ("qttools" ,qttools)
        ("ftgl" ,ftgl)
        ("rtmidi" ,rtmidi)))
    (arguments
     `(#:tests? #f)) ; No tests.
    (home-page "http://pianobooster.sourceforge.net")
    (synopsis "MIDI file player/game")
    (description
      "Piano Booster is a MIDI file player that displays the musical notes AND teaches you how to play the piano.  You can play along to any track in the midi file and PianoBooster will follow YOUR playing.  PianoBooster makes sight reading fun!")
    (license "license:gpl3+")))

pianobooster
