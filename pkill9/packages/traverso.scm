(define-module (pkill9 packages traverso)
  #:use-module (guix download)
  #:use-module (guix build-system cmake)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages))

(define-public traverso
  (package
   (name "traverso")
   (version "0.49.6")
   (source (origin
            (method url-fetch)
            (uri "https://traverso-daw.org/download/12")
            (file-name (string-append name "-" version ".tar.gz")) ;; Guix downloader doesn't use server-suggested filename, so manually specify it - this may cause problems if the compression method doesn't match the extension.
            (sha256
             (base32
              "12f7x8kw4fw1j0xkwjrp54cy4cv1ql0zwz2ba5arclk4pf6bhl7q"))))
   (build-system cmake-build-system)
   (native-inputs
    `(("pkg-config" ,(@ (gnu packages pkg-config) pkg-config))))
   (inputs
    `(("wavpack" ,(@ (gnu packages audio) wavpack))
      ("libvorbis" ,(@ (gnu packages xiph) libvorbis))
      ("flac" ,(@ (gnu packages xiph) flac))
      ("libmad" ,(@ (gnu packages mp3) libmad))
      ("fftw" ,(@ (gnu packages algebra) fftw))
      ("qtbase" ,(@ (gnu packages qt) qtbase))
      ("alsa-lib" ,(@ (gnu packages linux) alsa-lib))
      ("jack" ,(@ (gnu packages audio) jack-1))
      ("libsamplerate" ,(@ (gnu packages pulseaudio) libsamplerate)) ;; Required but not declared a requirement in cmake configuration file
      ("lilv" ,(@ (gnu packages audio) lilv)))) ;; Optional dependency
   (arguments
    `(#:tests? #f
      #:configure-flags (list "-DCXX_FLAGS=-std=c++11"))) ;; "CXX_FLAGS" is a custom internally used variable in CMakeLists.txt which is appended to CMAKE_CXX_FLAGS_{DEBUG,RELEASE} - CMAKE_CXX_FLAGS is overwritten in the file.
   (home-page "https://traverso-daw.org")
   (synopsis "Digital audio workstation")
   (description "Traverso-DAW is a digital audio workstation designed to be an extension of your creativity and workflow. Compose, record and edit your audio with an unprecedented speed and ease as you’ve never experienced before")
   (license license:gpl2)))

traverso
