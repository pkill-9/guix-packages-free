(define-module (pkill9 packages rigs-of-rods rigs-of-rods)
  #:use-module (pkill9 packages rigs-of-rods ogre-next) ; for ogre-1.11.5 and mygui/ogre-1.11.5
  #:use-module (gnu packages web) ; for rapidjson
  #:use-module (srfi srfi-1) ; for alist-delete
  #:use-module (guix build-system cmake)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (guix git-download)
  #:use-module (guix download)
 )


(define-public ogre-ror ; RoR-specific customizations of ogre
  (package (inherit ogre-1.11.5)
    (version (string-append "custom-for-rigs-of-rods-" (package-version ogre-1.11.5)))
    (arguments
      `(,@(substitute-keyword-arguments (package-arguments ogre-1.11.5)
                                        ((#:configure-flags cf)
                                         `(cons "-DOGRE_RESOURCEMANAGER_STRICT=0" ,cf)))));; needed by Rigs of Rods I think - yes it does, it gives me a sky and body texture!!
    ))

(define-public mygui-ror/ogre-ror ; compile mygui with ogre-ror, to prevent incompatibilities
  (package (inherit mygui/ogre-1.11.5)
	   (version "custom-for-rigs-of-rods-git")
    (inputs
     `(,@(alist-delete "ogre" (package-inputs mygui/ogre-1.11.5))
      ("ogre" ,ogre-ror)))
    ))

(define-public rapidjson-git ; Current stable version of rapidjson causes a crash when opening multiplayer lobby
  (package (inherit rapidjson)
    (version "git")
    (source
            (origin
              (method git-fetch)
             (uri (git-reference
                    (url "https://github.com/miloyip/rapidjson.git")
                    (commit "bfdcf4911047688fec49014d575433e2e5eb05be")))
              (sha256
                (base32
                 "1yi5996kpi80gh6fb3h5v7p4js1h9lxy8v1h4p8mn9cdyk6kiq6d"))))))

(define-public ogre-pagedgeometry ; Optional RoR dependency, currently not used because it's bundled in the RoR source.
  (package
    (name "ogre-pagedgeometry")
    (version "ror-git")
    (source
        (origin
          (method git-fetch)
         (uri (git-reference
                (url "https://github.com/RigsOfRods/ogre-pagedgeometry.git")
                (commit "a82d5222deb76b9c9042f13289206181902afa90")))
          (sha256
            (base32
             "1shil6hmqzw6if2brshaikfw9w1hfblyi0n42fyi6b2aklx3jwbm"))))
    (build-system cmake-build-system)
    (inputs
      `(("ogre" ,ogre-ror)))
    (arguments
     `(#:tests? #f)) ; No tests I assume
    (home-page "http://wiki.ogre3d.org/tiki-index.php?page=PagedGeometry+Engine")
    (synopsis "Paged Geometry is a plugin for OGRE for rendering of dense vegetation")
    (description "The PagedGeometry engine is an add-on to Ogre which provides highly optimized methods for rendering massive amounts of small meshes, covering a possibly infinite area. This is especially well suited for dense forests and outdoor scenes, with millions of trees, bushes, grass, rocks, etc.")
    (license #f)))

(define-public socketw ; Optional RoR dependency, provides network multiplayer
  (package
    (name "socketw")
    (version "ror-git")
    (source
        (origin
          (method git-fetch)
         (uri (git-reference
                (url "https://github.com/RigsOfRods/socketw.git")
                (commit "f8db4420c19508e559d4f500b64d1bad7534d85d")))
          (sha256
            (base32
             "1b89y19h1krfykssrarj3hx5fa9s0ki52zz1vr2n7xrvf1nb1805"))))
    (build-system cmake-build-system)
    (inputs
      `(("ogre" ,ogre-ror)
        ("openssl" ,(@ (gnu packages tls) openssl))))
    (arguments
     `(#:tests? #f)) ; No tests I assume
    (home-page "http://rigsofrods.github.io/socketw/")
    (synopsis "SocketW is a library which provides cross-platform socket abstraction")
    (description "SocketW is a cross platform (Linux/FreeBSD/Unix/Win32) streaming socket C++ library designed to be easy to use. It supports Unix sockets and TCP/IP sockets with optional SSL/TLS (OpenSSL) support. SocketW allows you to write portable and secure network applications quickly without needing to spend time learning low-level system functions or reading OpenSSL manuals.")
    (license #f)))


(define-public angelscript-anotherfoxguy ; Optional RoR dependency, provides game scripting capabilities.
  (package
    (name "angelscript")
    (version "anotherfoxguy-git-ror-dependencies-tag")
    (source
        (origin
          (method git-fetch)
         (uri (git-reference
                (url "https://github.com/AnotherFoxGuy/angelscript.git")
                (commit "c0e460930d4cb73bcfbeb253cd5611d7156b1cce"))); Same source and git commit as used in the 'ror-dependencies' script: https://github.com/RigsOfRods/ror-dependencies/commit/a4b7302b3457f78ec7a78bdb2359d35b8db557e3
          (sha256
            (base32
             "0ahv3ij5d9chvciwh0zyj9izams0f1yrfnbbkvjkcjscxh98lh9l"))))
  
    (build-system cmake-build-system)
    (arguments
     `(#:tests? #f)) ; No tests
    (home-page "http://www.angelcode.com/angelscript/")
    (synopsis "Flexible cross-platform scripting library designed to allow applications to extend their functionality through external scripts.")
    (description "The AngelCode Scripting Library, or AngelScript as it is also known, is an extremely flexible cross-platform scripting library designed to allow applications to extend their functionality through external scripts. It has been designed from the beginning to be an easy to use component, both for the application programmer and the script writer.")
    (license "zlib")))

(define %cotire
    (origin
      (method url-fetch)
      (uri (string-append
             "https://raw.githubusercontent.com/sakra/cotire/master/CMake/cotire.cmake"))
      (sha256
        (base32
          "1910pv6rgqxy1ni63r3zhbwcm1dnzs0h6h95h6qh1p371vj2by0q"))))

(define-public rigs-of-rods
  (package
    (name "rigs-of-rods")
    (version "git")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
      	 (url "https://github.com/RigsOfRods/rigs-of-rods.git")
      	 (commit "ed924f605d9a8fa1d85842cec188eff1b31b2474")
    	 (recursive? #t)))
        ;(patches (search-patches "rigs-of-rods-no-angelscript.patch")) ; Uncomment this line to build without angelscript
        (sha256
          (base32
            "0a6hq0qkb70w1x2kjdlczxsxf4fkkc9281f3842nhxgvjic4hy2n"))))
    (build-system cmake-build-system)
    (native-inputs
      `( ("pkg-config"
         ,(@ (gnu packages pkg-config) %pkg-config))
	("cotire" ,%cotire)))
    (inputs
      `(("mygui" ,mygui-ror/ogre-ror)
        ("libx11" ,(@ (gnu packages xorg) libx11))
        ("ois" ,(@ (gnu packages game-development) ois))
        ("openal" ,(@ (gnu packages audio) openal))
        ("ogre" ,ogre-ror)
        ("rapidjson" ,rapidjson-git)
	("angelscript" ,angelscript-anotherfoxguy) ; Optional dependency - provides scripting - currently required due to source code structure
        ("socketw" ,socketw) ; Optional dependency - provides network multiplayer
        ("openssl" ,(@ (gnu packages tls) openssl)) ; Only required when compiling with socketw
	("curl" ,(@ (gnu packages curl) curl)) ; Optional dependency - provides multiplayer lobby
	))
    (arguments
      `(#:tests? #f
	;#:configure-flags (list "-DROR_USE_ANGELSCRIPT=0") ; Uncomment this line to build without angelscript
	#:phases (modify-phases %standard-phases
				(add-after 'unpack 'install-cotire-to-source
					   (lambda _
					     (substitute* "CMakeLists.txt"
						(("CMAKE_BINARY_DIR}/cotire.cmake") "CMAKE_SOURCE_DIR}/cotire.cmake"))
					     (copy-file (assoc-ref %build-inputs "cotire")
							"cotire.cmake")
					     #t))
				(add-after 'build 'zip-and-copy-resources
					   (lambda _
					     (invoke "make" "zip_and_copy_resources")
					     #t))
				(replace 'install
					 (lambda _
				             (copy-recursively "bin"
							       (string-append (assoc-ref %outputs "out") "/opt/rigs-of-rods"))
					     (mkdir (string-append (assoc-ref %outputs "out") "/bin"))
					     (symlink (string-append (assoc-ref %outputs "out") "/opt/rigs-of-rods/RoR")
						      (string-append (assoc-ref %outputs "out") "/bin/RoR"))
					     #t))
				)))
    (home-page "http://rigsofrods.org")
    (synopsis "3D simulator game where you can drive, fly and sail various vehicles")
    (description "3D simulator game where you can drive, fly and sail various vehicles")
    (license "GPLv3")))


