(define-module (pkill9 packages rigs-of-rods ogre-next)
  #:use-module (guix build-system cmake)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (srfi srfi-1) ; for alist-delete
  #:use-module (gnu packages graphics) ; for ogre
  #:use-module (gnu packages game-development) ; for mygui
  #:use-module (gnu packages check) ; for googletest-source or something
  )

(define googletest-source
  (let* ((name "googletest-source")
         (version "1.8.0"))
    (origin
     (method url-fetch)
     (uri (string-append "https://github.com/google/googletest/archive/"
                         "release-" version ".tar.gz"))
     (file-name (string-append name "-" version ".tar.gz"))
     (sha256
      (base32
       "1n5p1m2m3fjrjdj752lf92f9wq3pl5cbsfrb49jqbg52ghkz99jq")))))

(define-public ogre-1.11.5
  (package
   (name "ogre")
   (version "1.11.5")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/OGRECave/ogre.git")
           (commit (string-append "v" version))))
     (sha256
      (base32
       "12s8d582nw27yn7z7vjrir9cbr419c7am7qbhzz1c0izv2jih69a"))))
   (build-system cmake-build-system)
   (native-inputs
    `(("boost" ,(@ (gnu packages boost) boost))
      ("doxygen"
       ,(@ (gnu packages documentation) doxygen))
      ("pkg-config"
       ,(@ (gnu packages pkg-config) %pkg-config))
      ("googletest-source"
       ,googletest-source)))
   (inputs
    `(("font-dejavu"
       ,(@ (gnu packages fonts) font-dejavu))
      ("freeimage" ,(@ (gnu packages image) freeimage))
      ("freetype"
       ,(@ (gnu packages fontutils) freetype))
      ("glu" ,(@ (gnu packages gl) glu))
      ("googletest"
       ,(@ (gnu packages check) googletest))
      ("sdl2" ,(@ (gnu packages sdl) sdl2))
      ("libxaw" ,(@ (gnu packages xorg) libxaw))
      ("libxrandr" ,(@ (gnu packages xorg) libxrandr))
      ("tinyxml" ,(@ (gnu packages xml) tinyxml))
      ("zziplib"
       ,(@ (gnu packages compression) zziplib))
      ))
   (arguments
    '(#:phases
      (modify-phases %standard-phases
                     (add-after 'unpack 'set-cmake-install-rpath
                                        ; They didn't make the installation runpath an option flag, so we need to patch the source instead
                                (lambda _
                                  (substitute* "CMakeLists.txt"
                                               (("set\\(CMAKE_INSTALL_RPATH .*")
                                                "set(CMAKE_INSTALL_RPATH \"${CMAKE_INSTALL_PREFIX}/${OGRE_LIB_DIRECTORY}:${CMAKE_INSTALL_PREFIX}/${OGRE_LIB_DIRECTORY}/OGRE\")\n"))
                                  #t))
                     (add-before 'configure 'pre-configure
                                 (lambda _
                                   ;; It expects googletest source to be downloaded and
                                   ;; be in a specific place.
                                   (substitute* "Tests/CMakeLists.txt"
                                                (("URL(.*)$" _ suffix)
                                                 (string-append "URL " suffix
                                                                "\t\tURL_HASH "
                                                                "MD5=16877098823401d1bf2ed7891d7dce36\n")))
                                   #t))
                     (add-before 'build 'pre-build
                                 (lambda* (#:key inputs #:allow-other-keys)
                                   (copy-file (assoc-ref inputs "googletest-source")
                                              (string-append (getcwd)
                                                             "/Tests/googletest-prefix/src/"
                                                             "release-1.8.0.tar.gz"))
                                   #t)))
      #:configure-flags
      (list "-DOGRE_BUILD_TESTS=TRUE"
            "-DOGRE_INSTALL_DOCS=TRUE"
                                        ;"-DOGRE_INSTALL_SAMPLES=TRUE" ;;keeps complaining that it can't find SDK files
                                        ;"-DOGRE_INSTALL_SAMPLES_SOURCE=TRUE" ;;keeps complaining that it can't find SDK files
            )))
   (home-page "http://www.ogre3d.org")
   (synopsis "Scene-oriented, flexible 3D engine written in C++")
   (description "OGRE (Object-Oriented Graphics Rendering Engine) is a scene-oriented, flexible 3D engine written in C++ designed to make it easier and more intuitive for developers to produce applications utilising hardware-accelerated 3D graphics.")
   (license "Expat")))

(define-public mygui/ogre-1.11.5 ; MyGUI compatible with ogre-1.11.5
  (package (inherit mygui)
	   (version "custom-for-rigs-of-rods-git")
           (source
            (origin
             (method git-fetch)
             (uri (git-reference
                   (url "https://github.com/MyGUI/mygui.git")
                   (commit "cf86abc685179017859f52f223468d10f8e0442c"))) ; Same git commit as used in the 'ror-dependencies' script: https://github.com/RigsOfRods/ror-dependencies/commit/a4b7302b3457f78ec7a78bdb2359d35b8db557e3
             (sha256
              (base32
               "1f4kgy49vcl8cr0y55lbzyviznj0v0mzkrz1m2wpjjmaiqnr3jsy"))))
           (inputs
            `(,@(alist-delete "ogre" (package-inputs mygui))
              ("ogre" ,ogre-1.11.5)))
           ))
