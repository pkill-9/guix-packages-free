;; deal with "qt5linguisttools" error, seems to be looking for it or something
;;add support for -DKIS_TABLET=on configure flag
;; possibly add python3 input for scripts that the shebang-changer warns about (that it couldn't find python3)

(define-module (pkill9 packages drawpile)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system cmake)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages image)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (gnu packages pkg-config))

(define-public drawpile
  (package
   (name "drawpile")
   (version "2.0.11")
   (source
    (origin
     (method url-fetch)
     (uri (string-append "https://drawpile.net/files/src/drawpile-" version ".tar.gz"))
     (sha256
      (base32
       "0h018rxhc0lwpqwmlihalz634nd0xaafk4p2b782djjd87irnjpk"))))
   (build-system cmake-build-system)
   (native-inputs
    `(("pkg-config" ,pkg-config)))
   (inputs
    `(("qtbase" ,qtbase)
      ("qtsvg" ,qtsvg)
      ("qtmultimedia" ,qtmultimedia)
      ("karchive" ,karchive)
      ("giflib" ,giflib) ;; Optional client dependency
      ("kdnssd" ,kdnssd) ;; Optional client dependency
      ("miniupnpc" ,(@ (gnu packages upnp) miniupnpc)) ;; Optional client dependency
      ("libmicrohttpd" ,(@ (gnu packages gnunet) libmicrohttpd)) ;; Optional server dependency
      ("libsodium" ,(@ (gnu packages crypto) libsodium)))) ;; Optional server dependency
   ;;("extra-cmake-modules" ,extra-cmake-modules) ;; For -DKIS_TABLET=on
   ;;("qtx11extras" ,qtx11extras) ;; For -DKIS_TABLET=on
   (arguments
    `(#:configure-flags '("-DTESTS=on" "-DCMAKE_BUILD_TYPE=Release" "-DTOOLS=on")))
   (home-page "https://drawpile.net")
   (synopsis "Drawpile is a Free software collaborative drawing program that allows multiple users to sketch on the same canvas simultaneously.")
   (description synopsis)
   (license license:gpl3)))
