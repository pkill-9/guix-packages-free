(define-module (pkill9 packages rockbox-utility)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages))

(define-public rockbox-utility
  (package
   (name "rockbox-utility")
   (version "1.4.0")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://download.rockbox.org"
                                "/rbutil/source/"
                                "RockboxUtility-v" version "-src.tar.bz2"))
            
            (sha256
             (base32
              "0k3ycga3b0jnj13whwiip2l0gx32l50pnbh7kfima87nq65aaa5w"))))
   (build-system gnu-build-system)
   (native-inputs ;; It wants pkg-config as well i think, but it still builds iwhtout it...
    `(("qttools" ,(@ (gnu packages qt) qttools))
      ("pkg-config" ,(@ (gnu packages pkg-config) pkg-config))
      ("bash" ,(@ (gnu packages bash) bash))))
   (inputs
    `(("qtbase" ,(@ (gnu packages qt) qtbase))
      ("libusb" ,(@ (gnu packages libusb) libusb))
      ("zlib" ,(@ (gnu packages compression) zlib))))
   (arguments
    `(#:parallel-build? #f ;; Fix rbutilqt-lang.cpp failure
      #:phases
      (modify-phases %standard-phases
                     (add-after 'unpack 'fix-lrelease-path ;; This fails to run the .pro file anyway, so just fix it to prevent it erroring
                                (lambda* (#:key inputs #:allow-other-keys)
                                  (let ((lrelease (string-append (assoc-ref inputs "qttools") "/bin/lrelease")))
                                    (substitute* "rbutil/rbutilqt/rbutilqt.pro"
                                                 (("lrelease\\.commands = \\$\\$\\[QT_INSTALL_BINS\\]/lrelease -silent")
                                                  (string-append "lrelease.commands = " lrelease))))))
                     (add-after 'unpack 'fix-install-path
                      (lambda* (#:key outputs #:allow-other-keys)
                        (let ((out (assoc-ref outputs "out")))
                          (substitute* "rbutil/rbutilqt/rbutilqt.pro"
                           (("target\\.path =.*") (string-append "target.path = " out "/bin" "\n"))))))
                     (replace 'configure
                              (lambda* (#:key inputs #:allow-other-keys)
                                (let ((lrelease (string-append (assoc-ref inputs "qttools") "/bin/lrelease"))
                                      (sh (string-append (assoc-ref inputs "bash") "/bin/sh"))
                                      (qmake (string-append (assoc-ref inputs "qtbase") "/bin/qmake")))
                                  (invoke sh "-c" (string-append lrelease " rbutil/rbutilqt/lang/*"))
                                  (invoke qmake "rbutil/rbutilqt/rbutilqt.pro"))))
                     )))
    (home-page "https://www.rockbox.org/")
    (synopsis "Rockbox is a free replacement firmware for digital music players. It runs on a wide range of players. This package is utility software for managing it.")
    (description synopsis)
    (license license:gpl2)))

  (package (inherit rockbox-utility))
