(define-module
  (pkill9 packages lbreakouthd)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages sdl))

(define-public lbreakouthd
  (package
    (name "lbreakouthd")
    (version "1.0.6")
    (source
      (origin
        (method url-fetch)
        (uri "https://sourceforge.net/projects/lgames/files/lbreakouthd/lbreakouthd-1.0.6.tar.gz")
        (sha256
          (base32 "1g8m5qkzxqgylgszvn23mn1qs5lsnjbpdmyzw4sbs86gigc8lpyz"))))
    (build-system gnu-build-system)
    (inputs
      `(("sdl2" ,sdl2)
        ("sdl2-image" ,sdl2-image)
        ("sdl2-mixer" ,sdl2-mixer)
        ("sdl2-ttf" ,sdl2-ttf)))
    (home-page "http://lgames.sourceforge.net/LBreakoutHD/")
    (synopsis "breakout")
    (description "breakout")
    (license license:gpl3)))

lbreakouthd
