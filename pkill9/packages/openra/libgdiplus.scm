(define-module (pkill9 packages openra libgdiplus)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses)
 )

; Example arguments to use:
;    (arguments
;     `(#:tests? #f
;       #:make-flags '("CC=gcc" "RM=rm" "SHELL=sh" "all")
;       #:phases (modify-phases %standard-phases (delete 'configure))
;       ))

(define-public libgdiplus
(package
  (name "libgdiplus")
  (version "5.6")
  (source
    (origin
      (method url-fetch)
      (uri (string-append
             "http://download.mono-project.com/sources/libgdiplus/libgdiplus-"
             version
             ".tar.gz"))
      (sha256
        (base32
          "0fn57m6jzqpwji5q3avdyg7k5sc2dj1bizki54d7plzq792c1wp1"))))
  (build-system gnu-build-system)
  (native-inputs
    `(("pkg-config"
       ,(@ (gnu packages pkg-config) %pkg-config))))
  (inputs
    `(("glib" ,(@ (gnu packages glib) glib))
      ("cairo" ,(@ (gnu packages gtk) cairo))
      ("fontconfig"
       ,(@ (gnu packages fontutils) fontconfig))
      ("libtiff" ,(@ (gnu packages image) libtiff))
      ("libjpeg" ,(@ (gnu packages image) libjpeg))
      ("libpng" ,(@ (gnu packages image) libpng))
      ("libxrender"
       ,(@ (gnu packages xorg) libxrender))
      ("libexif" ,(@ (gnu packages photo) libexif))
      ("giflib" ,(@ (gnu packages image) giflib))))
  ;WARNING: testgraphics and testpng tests fail!!
  (arguments
    `(#:tests? #f))
  (home-page "")
  (synopsis "")
  (description "")
  (license gpl3+))

 )
