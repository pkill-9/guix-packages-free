(define-module (pkill9 packages openra thirdparty)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system trivial)
  #:use-module (guix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages compression)
  ;#:use-module (gnu packages gettext)
 )

(define dotnet-package
  (package
   (name "dotnet-package")
   (version "0")
     ;https://nuget.org/api/v2/package/"$package"/"$version"
   (source #f)
   (build-system trivial-build-system)
   (native-inputs
    `(("unzip" ,unzip)))
   (arguments
   `(;#:modules ((guix build utils))
     #:builder
     (begin
       (let* ((out (assoc-ref %outputs "out"))
              (source (assoc-ref %build-inputs "source"))
              (unzip (string-append (assoc-ref %build-inputs "unzip") "/bin/unzip")))
         (zero? (system* unzip source "-d" out))))))

   (home-page "https://nuget.org")
   (synopsis "Nuget package")
   (description "Nuget package")
   (license #f)
   ))

(define-public geoip
  (package (inherit dotnet-package)
           (name "GeoLite2-Country")
           (version "0")
           (source
            (origin
             (method url-fetch)
             (uri (string-append "http://geolite.maxmind.com/download/geoip/database/" name ".mmdb.gz"))
             (sha256
              (base32
               "1wdw5jwr5hlaj5al9xxz0mxjq3p4aw398qb8dvqj6g4jixw012cf"))))
           (arguments
            `(#:modules ((guix build utils))
              #:builder
              (begin (use-modules ((guix build utils)))
                     (let* ((out (assoc-ref %outputs "out"))
                            (source (assoc-ref %build-inputs "source")))
                       (mkdir-p out)
                       (copy-file source (string-append out "/GeoLite2-Country.mmdb.gz"))))))
           ))

(define-public stylecop-plus
  (package (inherit dotnet-package)
    (name "StyleCopPlus")
    (version "4.7.49.5")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://nuget.org/api/v2/package/" name ".MSBuild" "/" version))
        (sha256
          (base32
            "1nc3fr9y5hs3rh7b8mgk7z8siwvgflmcp937syz0fsaq8jkjb0n9"))))
  ))

(define-public stylecop
  (package (inherit dotnet-package)
    (name "StyleCop")
    (version "4.7.49.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "https://nuget.org/api/v2/package/" name ".MSBuild" "/" version))
      (sha256
       (base32
        "1cjhsr473ij0dqr35vgkkswxddcmv4yc7gw6c6chh3vlgvcplz4d"))))
          ))

(define-public sharpziplib
  (package (inherit dotnet-package)
           (name "SharpZipLib")
           (version "0.86.0")
           (source
            (origin
             (method url-fetch)
             (uri (string-append "https://nuget.org/api/v2/package/" name "/" version))
             (sha256
              (base32
               "17kvg21ga22fv1cgvxsvia08rac3j3rpr9ncbf4wn9cf2ji5l13m"))))
           ))

(define-public maxmind
  (package (inherit dotnet-package)
           (name "MaxMind")
           (version "2.0.0")
           (source
            (origin
             (method url-fetch)
             (uri (string-append "https://nuget.org/api/v2/package/" name ".Db" "/" version))
             (sha256
              (base32
               "10f1wziphwzdvn71yh8q128prlzmajraf4a9p25zwdf746ymlncq"))))
           ))

(define-public sharpfont
  (package (inherit dotnet-package)
           (name "SharpFont")
           (version "4.0.1")
           (source
            (origin
             (method url-fetch)
             (uri (string-append "https://nuget.org/api/v2/package/" name "/" version))
             (sha256
              (base32
               "0gpxzx2jqgw069ac7q8w92w52pvq0nsarwypjg6s013k5w11ff5v"))))
           ))

(define-public nunit
  (package (inherit dotnet-package)
           (name "NUnit")
           (version "3.0.1")
           (source
            (origin
             (method url-fetch)
             (uri (string-append "https://nuget.org/api/v2/package/" name "/" version))
             (sha256
              (base32
               "0g22vfa5zy2sbwr3f1l6y8lqapwjvkb76c2prpbj49hl8imia2zb"))))
           ))

(define-public nunit-console
  (package (inherit dotnet-package)
           (name "NUnit.Console")
           (version "3.0.1")
           (source
            (origin
             (method url-fetch)
             (uri (string-append "https://nuget.org/api/v2/package/" name "/" version))
             (sha256
              (base32
               "0zz0mn8hgvxzd9k10qp8i58bw88swnffad4jlyack6kn9l9fjk0n"))))
           (arguments
            `(;#:modules ((guix build utils))
              #:builder
              (begin
                (let* ((out (assoc-ref %outputs "out"))
                       (source (assoc-ref %build-inputs "source"))
                       (unzip (string-append (assoc-ref %build-inputs "unzip") "/bin/unzip")))
                  (zero? (system* unzip source "-d" out))
                  (chmod (string-append out "/tools/nunit3-console.exe") #o755)))))
           ))

(define-public open-nat
  (package (inherit dotnet-package)
           (name "Open.Nat")
           (version "2.1.0")
           (source
            (origin
             (method url-fetch)
             (uri (string-append "https://nuget.org/api/v2/package/" name "/" version))
             (sha256
              (base32
               "1qp3y21y2700mim69by01jyscagqqgpsb5s7m8l7wjlb8zhxwcn5"))))
           ))

(define-public fuzzylogiclibrary
  (package (inherit dotnet-package)
           (name "FuzzyLogicLibrary")
           (version "1.2.0")
           (source
            (origin
             (method url-fetch)
             (uri (string-append "https://nuget.org/api/v2/package/" name "/" version))
             (sha256
              (base32
               "0gyz4ljnw3w3b3l2w6gpnkyd4l290fgmi5spvpgn3rksccrsz36a"))))
           ))

(define-public sdl2-cs
  (package (inherit dotnet-package)
           (name "SDL2-CS")
           (version "20161223")
           (source
            (origin
             (method url-fetch)
             (uri (string-append "https://github.com/OpenRA/" name "/releases/download/" version "/" name ".dll"))
             (sha256
              (base32
               "187pwjnrd2f7bfync1grjw6xirylfgcxyxpl21rb6k6if6ki8v5h"))))
           (arguments
            `(#:modules ((guix build utils))
              #:builder
              (begin (use-modules ((guix build utils)))
                     (let* ((out (assoc-ref %outputs "out"))
                            (source (assoc-ref %build-inputs "source")))
                       (mkdir-p out)
                       (copy-file source (string-append out "/SDL2-CS.dll"))))))
           ))

(define-public sdl2-cs-config
  (package (inherit dotnet-package)
           (name "SDL2-CS-config")
           (version "20161223")
           (source
            (origin
             (method url-fetch)
             (uri (string-append "https://github.com/OpenRA/" "SDL2-CS" "/releases/download/" version "/" "SDL2-CS" ".dll.config"))
             (sha256
              (base32
               "15709iscdg44wd33szw5y0fdxwvqfjw8v3xjq6a0mm46gr7mkw7g"))))
           (arguments
            `(#:modules ((guix build utils))
              #:builder
              (begin (use-modules ((guix build utils)))
                     (let* ((out (assoc-ref %outputs "out"))
                            (source (assoc-ref %build-inputs "source")))
                       (mkdir-p out)
                       (copy-file source (string-append out "/SDL2-CS.dll.config"))))))
           ))

(define-public openal-cs
  (package (inherit dotnet-package)
           (name "OpenAL-CS")
           (version "20151227")
           (source
            (origin
             (method url-fetch)
             (uri (string-append "https://github.com/OpenRA/" name "/releases/download/" version "/" name ".dll"))
             (sha256
              (base32
               "0lvyjkn7fqys97wym8rwlcp6ay2z059iabfvlcxhlrscjpyr2cyk"))))
           (arguments
            `(#:modules ((guix build utils))
              #:builder
              (begin (use-modules ((guix build utils)))
                     (let* ((out (assoc-ref %outputs "out"))
                            (source (assoc-ref %build-inputs "source")))
                       (mkdir-p out)
                       (copy-file source (string-append out "/OpenAL-CS.dll"))))))
           ))

(define-public openal-cs-config
  (package (inherit dotnet-package)
           (name "OpenAL-CS-config")
           (version "20151227")
           (source
            (origin
             (method url-fetch)
             (uri (string-append "https://github.com/OpenRA/" "OpenAL-CS" "/releases/download/" version "/" "OpenAL-CS" ".dll.config"))
             (sha256
              (base32
               "0wcmk3dw26s93598ck5jism5609v0y233i0f1b76yilyfimg9sjq"))))
           (arguments
            `(#:modules ((guix build utils))
              #:builder
              (begin (use-modules ((guix build utils)))
                     (let* ((out (assoc-ref %outputs "out"))
                            (source (assoc-ref %build-inputs "source")))
                       (mkdir-p out)
                       (copy-file source (string-append out "/OpenAL-CS.dll.config"))))))
           ))

(define-public eluant
  (package (inherit dotnet-package)
           (name "Eluant")
           (version "20160124")
           (source
            (origin
             (method url-fetch)
             (uri (string-append "https://github.com/OpenRA/" name "/releases/download/" version "/" name ".dll"))
             (sha256
              (base32
               "0cfzb0dhgvczskgnf6bkv4pnbg7f7c966d9lnm3dfi2b8ajlx311"))))
           (arguments
            `(#:modules ((guix build utils))
              #:builder
              (begin (use-modules ((guix build utils)))
                     (let* ((out (assoc-ref %outputs "out"))
                            (source (assoc-ref %build-inputs "source")))
                       (mkdir-p out)
                       (copy-file source (string-append out "/Eluant.dll"))))))
           ))

(define-public rix0rrr-beaconlib
  (package (inherit dotnet-package)
           (name "rix0rrr.BeaconLib")
           (version "1.0.1")
           (source
            (origin
             (method url-fetch)
             (uri (string-append "https://nuget.org/api/v2/package/" name "/" version))
             (sha256
              (base32
               "1v3ly28xjhzqb32c56v3ncmx8k5glpwxwd8vngya4lxgfvql8kgd"))))
           ))
