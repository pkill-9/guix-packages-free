(define-module (pkill9 packages openra openra)
  #:use-module (pkill9 packages openra thirdparty)
  #:use-module (pkill9 packages openra libgdiplus)
  #:use-module (pkill9 packages openra ca-certificates-mono)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages mono)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  ;#:use-module (gnu packages gettext)
 )

(define-public openra
  (package
    (name "openra")
    (version "20190314")
    (source
     (origin
      (method url-fetch)
      ;(uri (string-append "https://github.com/OpenRA/OpenRA/releases/download/release-" version "/OpenRA-release-" version "-source.tar.bz2"))
      (uri (string-append "https://github.com/OpenRA/OpenRA/archive/release-20190314.tar.gz"))
      (sha256
       (base32
        "059vw5qm3qm2iz6kvv4c0bz4icd35bpfbql5zg3b92fkqly3npcy"))))
    (build-system gnu-build-system)
    (native-inputs
     `(("pkg-config" ,pkg-config)))
       ;("git" ,git))) ;only works in git repo
    (inputs
     `(("mono" ,mono)
       ("sdl2" ,sdl2)
       ("lua" ,lua-5.1)
       ("freetype" ,freetype)
       ("openal", openal)
       ("zenity" ,zenity)
       ("which" ,which)
       ("python" ,python-2)
       ("libgdiplus" ,libgdiplus)
       ("ca-certificates-mono" ,ca-certificates-mono)
     ;thirdparty
       ("geoip" ,geoip)
       ("stylecop-plus" ,stylecop-plus)
       ("stylecop" ,stylecop)
       ("sharpziplib" ,sharpziplib)
       ("maxmind" ,maxmind)
       ("sharpfont" ,sharpfont)
       ("nunit" ,nunit)
       ("nunit-console" ,nunit-console)
       ("open-nat" ,open-nat)
       ("fuzzylogiclibrary" ,fuzzylogiclibrary)
       ("sdl2-cs" ,sdl2-cs)
       ("sdl2-cs-config" ,sdl2-cs-config)
       ("openal-cs" ,openal-cs)
       ("openal-cs-config" ,openal-cs-config)
       ("eluant" ,eluant)
       ("rix0rrr-beaconlib" ,rix0rrr-beaconlib)
       ))
    (arguments
     `(#:tests? #f
       #:make-flags (list "version" "prefix=" (string-append "DESTDIR=" %output))
       #:phases (modify-phases %standard-phases
                  (delete 'configure)
                  (add-after 'unpack 'set-version
                             (lambda _
                               (substitute* "Makefile"
                                 (("^VERSION.*") (string-append "VERSION = release-20190314"))))) ; !!!!! NEED TO MAKE IT SUBSTITUTE SPECIFIED VERSION !!!!!
                  (add-before 'build 'attach-thirdparty-dependencies
                   (lambda _
                     (define (install-depfile package filepath)
                       (display (string-append "Getting third-party dependency: " (assoc-ref %build-inputs package) "/" filepath "\n"))
                       (install-file (string-append (assoc-ref %build-inputs package) "/" filepath) "thirdparty/download"))
                     (mkdir-p "thirdparty/download")
                     (install-depfile "geoip" "GeoLite2-Country.mmdb.gz")
                     (install-depfile "stylecop-plus" "tools/StyleCopPlus.dll")
                     ;(install-depfile "stylecop" "tools/StyleCop*.dll")
                     (install-depfile "stylecop" "tools/StyleCop.CSharp.dll")
                     (install-depfile "stylecop" "tools/StyleCop.CSharp.Rules.dll")
                     (install-depfile "stylecop" "tools/StyleCop.dll")
                     (install-depfile "sharpziplib" "lib/20/ICSharpCode.SharpZipLib.dll")
                     ;(install-depfile "maxmind" "lib/net45/MaxMind.Db.*")
                     (install-depfile "maxmind" "lib/net45/MaxMind.Db.dll")
                     (install-depfile "maxmind" "lib/net45/MaxMind.Db.xml")
                     ;(install-depfile "sharpfont" "lib/net45/SharpFont*")
                     (install-depfile "sharpfont" "lib/net45/SharpFont.dll")
                     (install-depfile "sharpfont" "lib/net45/SharpFont.dll.config")
                     (install-depfile "sharpfont" "lib/net45/SharpFont.xml")
                     ;(install-depfile "nunit" "lib/net40/nunit.framework*")
                     (install-depfile "nunit" "lib/net40/nunit.framework.dll")
                     (install-depfile "nunit" "lib/net40/nunit.framework.xml")
                     ;(install-depfile "nunit-console" "tools/*")
                     (copy-recursively (string-append (assoc-ref %build-inputs "nunit-console") "/tools") "thirdparty/download")
                     (install-depfile "open-nat" "lib/net45/Open.Nat.dll")
                     (install-depfile "fuzzylogiclibrary" "bin/Release/FuzzyLogicLibrary.dll")
                     (install-depfile "sdl2-cs" "SDL2-CS.dll")
                     (install-depfile "sdl2-cs-config" "SDL2-CS.dll.config")
                     (install-depfile "openal-cs" "OpenAL-CS.dll")
                     (install-depfile "openal-cs-config" "OpenAL-CS.dll.config")
                     (install-depfile "eluant" "Eluant.dll")
                     (install-depfile "rix0rrr-beaconlib" "lib/net40/rix0rrr.BeaconLib.dll")
                     ))
                  (add-before 'build 'provide-lua-location
                              (lambda _
                              (substitute* "thirdparty/configure-native-deps.sh"
                                           (("locations=.*") (string-append "locations=" (assoc-ref %build-inputs "lua") "/lib" "\n")))))
                  (add-after 'install 'provide-absolute-binary-path
                              (lambda _
                                (substitute* (string-append (assoc-ref %outputs "out") "/lib/openra/launch-game.sh")
                                             (("OpenRA\\.Game\\.exe") (string-append (assoc-ref %outputs "out") "/lib/openra/OpenRA.Game.exe")))))
                  (add-after 'provide-absolute-binary-path 'wrap-binary
                             (lambda _
                             (wrap-program (string-append (assoc-ref %outputs "out") "/lib/openra/launch-game.sh")
                                           `("PATH" = (,(string-append (assoc-ref %build-inputs "zenity") "/bin")
                                                       ,(string-append (assoc-ref %build-inputs "which") "/bin")
                                                       ,(string-append (assoc-ref %build-inputs "python") "/bin")
                                                       ,(string-append (assoc-ref %build-inputs "mono") "/bin")))
                                           `("LD_LIBRARY_PATH" = (,(string-append (assoc-ref %build-inputs "sdl2") "/lib")
                                                                  ,(string-append (assoc-ref %build-inputs "freetype") "/lib")
                                                                  ,(string-append (assoc-ref %build-inputs "openal") "/lib")
                                                                  ,(string-append (assoc-ref %build-inputs "lua") "/lib")
                                                                  ,(string-append (assoc-ref %build-inputs "libgdiplus") "/lib")
                                                                  ))
                                           `("XDG_CONFIG_HOME" = (,(assoc-ref %build-inputs "ca-certificates-mono"))) ; workaround to get mono to use specified certificates store, see github issue mentioned at the top. Not long-term solution because openra may try to use the .config directory (and/or other runtime dependencies), however it seems to only use the ~/.openra directory.
                               )))
		  (add-after 'wrap-binary 'add-bin-link
			     (lambda _
			       (mkdir-p (string-append (assoc-ref %outputs "out") "/bin"))
			       (symlink (string-append (assoc-ref %outputs "out") "/lib/openra/launch-game.sh") (string-append (assoc-ref %outputs "out") "/bin/openra"))))
       )))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))
