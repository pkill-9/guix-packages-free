(define-module (pkill9 packages openra ca-certificates-mono)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages mono)
  #:use-module (gnu packages certs)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (gnu packages base)
  #:use-module (guix build utils)
  #:use-module (guix build-system trivial))


(define-public ca-certificates-mono
  (package
    (name "ca-certificates-mono")
    (version "0")
    (source (local-file "/etc/ssl/certs/ca-certificates.crt"))
    (build-system trivial-build-system)
    (native-inputs
      `(("mono" ,mono)))
    (arguments
      `(#:modules ((guix build utils))
	#: builder (begin
		     (use-modules (guix build utils))
                     (let ((out (assoc-ref %outputs "out"))
			   (crt-file (assoc-ref %build-inputs "source"))
                           (cert-sync (string-append (assoc-ref %build-inputs "mono") "/bin/cert-sync")))
		           (mkdir out)
			   (setenv "XDG_CONFIG_HOME" out)
			   (zero? (system* cert-sync "--user" crt-file))))))
    (home-page "")
    (synopsis
      "")
    (description
      "")
    (license #f)))
