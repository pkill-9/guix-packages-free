(define-module (pkill9 packages twtxt)
 #: use-module (guix packages)
 #: use-module ((guix licenses) #:prefix license:)
 #: use-module (guix utils)
 #: use-module (guix download)
 #: use-module (gnu packages time)
 #: use-module (gnu packages python-web)
 #: use-module (gnu packages python-xyz)
 #: use-module (guix build-system python))

(define python-async-timeout
(package
  (name "python-async-timeout")
  (version "2.0.0")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "async-timeout" version))
      (sha256
        (base32
          "0rm5vsq71ay0m1lggv24m7skijd6hwknzmrp4yk9mm9msz18lzf1"))))
  (build-system python-build-system)
  (arguments `(#:tests? #f))
  (home-page
    "https://github.com/aio-libs/async_timeout/")
  (synopsis
    "Timeout context manager for asyncio programs")
  (description
    "Timeout context manager for asyncio programs")
  (license #f)))

(define python-chardet
(package
  (name "python-chardet")
  (version "3.0.4")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "chardet" version))
      (sha256
        (base32
          "1bpalpia6r5x1kknbk11p1fzph56fmmnp405ds8icksd3knr5aw4"))))
  (build-system python-build-system)
  (arguments `(#:tests? #f))
  (home-page "https://github.com/chardet/chardet")
  (synopsis
    "Universal encoding detector for Python 2 and 3")
  (description
    "Universal encoding detector for Python 2 and 3")
  (license #f)))
(define python-idna
(package
  (name "python-idna")
  (version "2.6")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "idna" version))
      (sha256
        (base32
          "13qaab6d0s15gknz8v3zbcfmbj6v86hn9pjxgkdf62ch13imssic"))))
  (build-system python-build-system)
  (arguments `(#:tests? #f))
  (home-page "https://github.com/kjd/idna")
  (synopsis
    "Internationalized Domain Names in Applications (IDNA)")
  (description
    "Internationalized Domain Names in Applications (IDNA)")
  (license #f)))

;tar: idna_ssl-1.0.0/requirements.txt: Not found in archive
;tar: Exiting with failure status due to previous errors
;guix import: warning: 'tar xf' failed with exit code 512
(define python-idna-ssl
(package
  (name "python-idna-ssl")
  (version "1.0.0")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "idna_ssl" version))
      (sha256
        (base32
          "0ix19qag51qz0bxl69i27mi6snc12akbpzgav8mf0cdx750f89qj"))))
  (build-system python-build-system)
  (arguments `(#:tests? #f))
  (propagated-inputs
    `(("python-idna" ,python-idna)))
  (home-page
    "https://github.com/aio-libs/idna_ssl")
  (synopsis
    "Patch ssl.match_hostname for Unicode(idna) domains support")
  (description
    "Patch ssl.match_hostname for Unicode(idna) domains support")
  (license #f)))

(define python-multidict
(package
  (name "python-multidict")
  (version "4.1.0")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "multidict" version))
      (sha256
        (base32
          "0liazqlyk2nmr82nhiw2z72j7bjqxaisifkj476msw140d4i4i7v"))))
  (build-system python-build-system)
  (arguments `(#:tests? #f))
  (home-page
    "https://github.com/aio-libs/multidict/")
  (synopsis "multidict implementation")
  (description "multidict implementation")
  (license #f)))

(define python-yarl
(package
  (name "python-yarl")
  (version "1.1.0")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "yarl" version))
      (sha256
        (base32
          "162630v7f98l27h11msk9416lqwm2mpgxh4s636594nlbfs9by3a"))))
  (build-system python-build-system)
  (arguments `(#:tests? #f))
  (propagated-inputs
    `(("python-idna-ssl" ,python-idna-ssl)
      ("python-idna" ,python-idna)
      ("python-multidict" ,python-multidict)))
  (home-page "https://github.com/aio-libs/yarl/")
  (synopsis "Yet another URL library")
  (description "Yet another URL library")
  (license #f)))

(define python-aiohttp
(package
  (name "python-aiohttp")
  (version "2.3.10")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "aiohttp" version))
      (sha256
        (base32
          "0r0955ar0yiwbp3war35z8zncs01nq84wdwk0v3s8f547dcadpca"))))
  (build-system python-build-system)
  (arguments `(#:tests? #f))
  (propagated-inputs
    `(("python-async-timeout" ,python-async-timeout)
      ;("python-chardet" ,python-chardet)
      ("python-kitchen" ,python-kitchen) ; rtv uses python-kitchen, which includes python-chardet, and these propagated inputs conflict, so we just use python-kitchen so it produces the same derivation.
      ("python-idna-ssl" ,python-idna-ssl)
      ("python-multidict" ,python-multidict)
      ("python-yarl" ,python-yarl)))
  (home-page
    "https://github.com/aio-libs/aiohttp/")
  (synopsis
    "Async http client/server framework (asyncio)")
  (description
    "Async http client/server framework (asyncio)")
  (license #f)))

(define python-click
(package
  (name "python-click")
  (version "6.7")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "click" version))
      (sha256
        (base32
          "02qkfpykbq35id8glfgwc38yc430427yd05z1wc5cnld8zgicmgi"))))
  (build-system python-build-system)
  (arguments `(#:tests? #f))
  (home-page "http://github.com/mitsuhiko/click")
  (synopsis
    "A simple wrapper around optparse for powerful command line utilities.")
  (description
    "A simple wrapper around optparse for powerful command line utilities.")
  (license #f)))

;tar: humanize-0.5.1/requirements.txt: Not found in archive
;tar: Exiting with failure status due to previous errors
;guix import: warning: 'tar xf' failed with exit code 512
(define python-humanize
(package
  (name "python-humanize")
  (version "0.5.1")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "humanize" version))
      (sha256
        (base32
          "06dvhm3k8lf2rayn1gxbd46y0fy1db26m3h9vrq7rb1ib08mfgx4"))))
  (build-system python-build-system)
  (arguments `(#:tests? #f))
  (home-page "http://github.com/jmoiron/humanize")
  (synopsis "python humanize utilities")
  (description "python humanize utilities")
  (license license:expat)))

(define python-dateutil
  (package
    (name "python-dateutil")
    (version "2.6.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "python-dateutil" version))
        (sha256
          (base32
            "1jkahssf0ir5ssxc3ydbp8cpv77limn8d4s77szb2nrgl2r3h749"))))
    (build-system python-build-system)
    (propagated-inputs `(("python-six" ,python-six)))
    (home-page "https://dateutil.readthedocs.io")
    (synopsis
      "Extensions to the standard Python datetime module")
    (description
      "Extensions to the standard Python datetime module")
    (license #f)))

(define-public twtxt
(package
  (name "twtxt")
  (version "1.2.3")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "twtxt" version))
      (sha256
        (base32
          "04myhzb171il16flvi84cqn84v4gczd55iagbx7lz01jdjs9a4dy"))))
  (build-system python-build-system)
  (arguments `(#:tests? #f))
  (propagated-inputs
    `(("python-aiohttp" ,python-aiohttp)
      ("python-click" ,python-click)
      ("python-dateutil" ,python-dateutil)
      ("python-humanize" ,python-humanize)))
  (home-page "https://github.com/buckket/twtxt")
  (synopsis
    "Decentralised, minimalist microblogging service for hackers.")
  (description
    "Decentralised, minimalist microblogging service for hackers.")
  (license #f)))
