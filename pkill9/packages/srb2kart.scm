;;Todo: Add a wrapper to run with opengl, for debugging.
;;Todo: Don't wrap symlink
;;Todo: patch upstream so it searches WADS in multiple share directories

(define-module (pkill9 packages srb2kart)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (guix build utils)
  #:use-module (guix download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages upnp)
  #:use-module (gnu packages music)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages image)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages gcc))

;; Get the data files from the windows installer, because I can't find them anywhere else and this is what the AUR package does. This package includes the 3D models.
(define srb2kart-data
  (package
   (name "srb2kart-data")
   (version "1.3")
      (source (origin
            (method url-fetch)
            (uri "https://github.com/STJr/Kart-Public/releases/download/v1.3/srb2kart-v13-Installer.exe")
            (sha256
             (base32
              "0bk36y7wf6xfdg6j0b8qvk8671hagikzdp5nlfqg478zrj0qf6cs"))))
   (build-system trivial-build-system)
   (native-inputs
    `(("atool" ,atool) ; Use atool because it unpacks subdirectories properly, not dumping their contents everywhere.
      ("p7zip" ,p7zip)))
   (arguments
    `(#:modules ((guix build utils))
      #:builder
      (begin
        (use-modules (guix build utils))
        (let ((out (assoc-ref %outputs "out"))
              (source (assoc-ref %build-inputs "source"))
              (atool (string-append (assoc-ref %build-inputs "atool") "/bin/atool"))
              (p7zip (string-append (assoc-ref %build-inputs "p7zip"))))
          (setenv "PATH" (string-append p7zip "/bin"))
          (invoke atool "-F7z" "-X" (getcwd) source)
          (copy-recursively "mdls" (string-append out "/share/srb2kart/mdls"))
          (for-each (lambda (file)
                      (install-file file (string-append out "/share/srb2kart")))
                    '("srb2.srb"
                      "mdls.dat"
                      "bonuschars.kart"
                      "chars.kart"
                      "gfx.kart"
                      "maps.kart"
                      "music.kart"
                      "patch.kart"
                      "sounds.kart"
                      "textures.kart"))))))
   (home-page "https://lightdash.org/kart/assets/")
   (synopsis "Data files for srb2kart")
   (description synopsis)
   (license "Unknown")))

(define-public srb2kart
  (package
   (name "srb2kart")
   (version "1.3")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/STJr/Kart-Public.git")
                  (commit (string-append "v" version))))
            (file-name (git-file-name name version))
            ;;(patches `(,%srb2kart-patch))
            (modules '((guix build utils)))
            (snippet
             '(begin
                (substitute* "src/m_menu.c"
                             (("compareval = cv_usejoystick.value") "compareval = cv_usejoystick.value;"))
                (substitute* "src/mserv.c"
                             (("different = \\( MSId != MSRegisteredId \\)") "different"))
                (substitute* "src/sdl/ogl_sdl.c"
                             (("const char \\*gllogdir = NULL;") "const char *gllogdir = NULL;\n        const char *gllogstream = NULL;"))))
            (sha256
             (base32
              "131g9bmc9ihvz0klsc3yzd0pnkhx3mz1vzm8y7nrrsgdz5278y49"))))
   (native-inputs
    `(("pkg-config" ,pkg-config)))
   (inputs
    `(("bash" ,bash) ; To wrap SRB2WADDIR.
      ;;("gcc" ,gcc-9)
      ("sdl2" ,sdl2)
      ("sdl2-mixer" ,sdl2-mixer)
      ("libupnp" ,libupnp)
      ("libgme" ,libgme)
      ("curl" ,curl)
      ("zlib" ,zlib)
      ("libpng" ,libpng)
      ("srb2kart-data" ,srb2kart-data)
      ("glu" ,glu)))
   (arguments
    `(#:tests? #f ; No tests.
      #:make-flags `("CC=gcc"
                     "LINUX64=1"
                     ;;,(string-append "CFLAGS="
                     ;;   l
                     ;;   (string-append "-I" (assoc-ref %build-inputs "sdl2-mixer") "/include/SDL2")
                     ;;   (string-append "-I" (assoc-ref %build-inputs "libgme") "/include/gme"))
                     )
      #:phases
      (modify-phases
       %standard-phases
       (delete 'configure) ;; No configure file
       (add-before 'build 'changedir
		   (lambda _
		     (chdir "src")))
       (add-after 'unpack 'fix-opengl-renderer ; Fixes failure when loading unwriteable assets when using opengl renderer using this fix: https://git.magicalgirl.moe/KartKrew/Kart-Public/issues/4. Doing this in a snippet changes the source directory name, which screws things up, so do it in a build phase instead.
                  (lambda _
                    (substitute* "src/hardware/r_opengl/r_opengl.c"
                                 (("#ifdef DEBUG_TO_FILE") "#if 0"))
                    ))
       (add-after 'unpack 'add-data-files
                  (lambda* (#:key inputs #:allow-other-keys)
                    (symlink (string-append (assoc-ref inputs "srb2kart-data")
                                            "/share/srb2kart")
                             "assets/installer")
                    #t))
       (add-before 'build 'set-cpath
                   (lambda* (#:key inputs #:allow-other-keys)
                     (setenv "C_INCLUDE_PATH"
                             (string-append
                              (getenv "C_INCLUDE_PATH") ":"
                              (assoc-ref inputs "sdl2-mixer") "/include/SDL2"))
                     #t))
       (replace 'install
                (lambda* (#:key outputs #:allow-other-keys)
                  (let ((out (assoc-ref outputs "out")))
                    (mkdir-p (string-append out "/bin"))
                    (display (string-append "======!!!!!!!!!!!========= PWD:" (getcwd) "========!!!!!!!========"))
                    (copy-file "../bin/Linux64/Release/lsdl2srb2kart" (string-append out "/bin/srb2kart"))
                    #t)))
       (add-after 'install 'wrap-binary
                  (lambda* (#:key inputs outputs #:allow-other-keys)
                    (let ((out (assoc-ref outputs "out"))
                          (srb2kart-data (assoc-ref inputs "srb2kart-data")))
                      (for-each (lambda (file)
                                  (wrap-program (string-append file)
                                                `("SRB2WADDIR" = (,(string-append srb2kart-data
                                                                                  "/share/srb2kart")))))
                                (find-files (string-append out "/bin")))
                      #t)))
       (add-after 'install 'install-desktop-files
                  (lambda* (#:key inputs outputs #:allow-other-keys)
                    (let ((out (assoc-ref outputs "out"))
                          (glu-lib (string-append (assoc-ref inputs "glu") "/lib/libGLU.so")))
                      (make-desktop-entry-file (string-append out "/share/applications/srb2kart.desktop")
                                               #:name "Sonic Robo Blast 2 Kart"
                                               #:exec (string-append out "/bin/srb2kart")
                                               #:icon "srb2"
                                               #:comment "A fan-made Racing game built on a modified DOOM engine, featuring characters from the world of Sonic"
                                               #:categories "Game"
                                               #:keywords "game;racing;racer;retro")
                      (make-desktop-entry-file (string-append out "/share/applications/srb2kart-opengl.desktop")
                                               #:name "Sonic Robo Blast 2 Kart (OpenGL)"
                                               #:exec (string-append out "/bin/srb2kart -opengl -GLUlib " glu-lib)
                                               #:icon "srb2"
                                               #:comment "A fan-made Racing game built on a modified DOOM engine, featuring characters from the world of Sonic"
                                               #:categories "Game"
                                               #:keywords "game;racing;racer;retro"))))
;;       (add-after 'install 'install-icon
;;                  (lambda* (#:key outputs #:allow-other-keys)
;;                    (install-file "../source/srb2.png"
;;                                  (string-append (assoc-ref outputs "out")
;;                                                 "/share/icons/hicolor/256x256/apps"))))
       (add-before 'install-license-files 'install-documentation
                   (lambda* (#:key outputs #:allow-other-keys)
                     (copy-recursively "../source/doc"
                                       (string-append (assoc-ref %outputs "out")
                                                      "/share/doc/srb2kart-" ,version))
                     #t)))))
   (build-system gnu-build-system)
   (home-page "https://mb.srb2.org/showthread.php?t=43708")
   (synopsis "Sprite-style racer featuring characters from the Sonic world.")
   (description "A racing mod for the fan-made game Sonic Robo Blast 2, which is based on the DOOM engine.")
   (license license:gpl2)))



(define-public srb2kart-birdhouse
  ;;patch-shebang: ./libs/zlib/zlib2ansi: warning: no binary for interpreter `perl' found in $PATH
  ;;make: [Makefile:632: pre-build] Error 1 (ignored)
  (package (inherit srb2kart)
           (name "srb2kart-birdhouse")
           (source
            (origin
             (method git-fetch)
             (uri (git-reference
                   (url "https://git.magicalgirl.moe/bird/Kart-Public.git")
                   (commit "a4d112383706b2f9fb7cfaedc0c944d91b3eb41e")))
             (file-name (git-file-name name (package-version srb2kart)))
             (sha256
              (base32
               "06mch3adc71j9cjm8ypw02q8ajnfcs7g2h5cznzvji7dxrx1c08r"))))
           (build-system gnu-build-system)
           (native-inputs
            `(,@(package-native-inputs srb2kart)
              ("pkg-config" ,pkg-config)))
           (arguments
            `(#:tests? #f ; No tests.
              #:make-flags '("LINUX64=1" "-C" "src")
              ,@(substitute-keyword-arguments (package-arguments srb2kart)
                                              ((#:phases phases)
                                               `(modify-phases ,phases
                                                               (delete 'configure) ; No configure file.
                                                               (delete 'fix-opengl-renderer) ; Breaks build with undefined references. Also opengl renderer isn't broken with unwriteable assets in the birdhouse release.
                                                               (add-before 'build 'set-cc
                                                                           (lambda* (#:key inputs #:allow-other-keys)
                                                                             (setenv "CC"
                                                                                     (string-append (assoc-ref inputs "gcc")
                                                                                                    "/bin/gcc"))
                                                                             #t))
                                                               (add-before 'build 'set-cpath
                                                                           (lambda* (#:key inputs #:allow-other-keys)
                                                                             (setenv "CPATH"
                                                                                     (string-append
                                                                                      (assoc-ref inputs "sdl2-mixer") "/include/SDL2"))
                                                                             #t))
                                                               (replace 'install
                                                                        (lambda* (#:key outputs #:allow-other-keys)
                                                                          (mkdir-p (string-append (assoc-ref outputs "out")
                                                                                                  "/bin"))
                                                                          (copy-file "bin/Linux64/Release/lsdl2srb2kart"
                                                                                     (string-append (assoc-ref outputs "out")
                                                                                                    "/bin/srb2kart"))))
                                                               )))))))

srb2kart
