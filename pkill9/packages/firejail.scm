(define-module (pkill9 packages firejail)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses))


(define-public firejail
(package
  (name "firejail")
  (version "0.9.64.4")
  (source
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/netblue30/firejail")
            (commit version)))
      (sha256
        (base32
          "0fqbam25gh62bpwmda2l6kb9ln818jdi5m76fqbma7p7rkqwpxdb"))))
  (build-system gnu-build-system)
  (arguments
   `(#:tests? #f
     ;;#:make-flags '("CC=gcc" "RM=rm" "SHELL=sh" "all")
     ;#:phases (modify-phases %standard-phases (delete 'configure))
     ))
  (home-page "https://firejail.wordpress.com")
  (synopsis "Sandboxing application")
  (description "Firejail is a SUID program that reduces the risk of security breaches by restricting the running environment of untrusted applications using Linux namespaces and seccomp-bpf.  It allows a process and all its descendants to have their own private view of the globally shared kernel resources, such as the network stack, process table, mount table.")
  (license gpl3+)))

firejail
