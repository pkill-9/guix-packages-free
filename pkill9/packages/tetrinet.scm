(define-module (pkill9 packages tetrinet)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses))

(define-public tetrinet
  (package
    (name "tetrinet")
    (version "0.11")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "http://tetrinet.or.cz/download/tetrinet-"
             version
             ".tar.bz2"))
       (sha256
        (base32
         "0b4pddqz6is1771qmvcj8qqlr4in2djdbkk13agvp9yhfah2v8x7"))))
    (build-system gnu-build-system)
    (inputs
     `(("ncurses" ,(@ (gnu packages ncurses) ncurses))))
    (arguments
     `(#:tests? #f 
       #:make-flags '("CC=gcc")
       #:phases (modify-phases %standard-phases
		  (delete 'configure)
		  (add-after 'unpack 'fix-install-dir
		    (lambda* (#:key outputs #:allow-other-keys)
                      (let ((out (assoc-ref outputs "out")))
			(mkdir-p (string-append out "/bin"))
			(substitute* "Makefile"
                          (("/usr/games") (string-append out "/bin")))))))))
    (home-page "http://tetrinet.or.cz")
    (synopsis "Multiplayer tetris")
    (description "Tetrinet is a multiplayer tetris game with powerups and
attacks you can use on opponents.")
    (license #f)))

(package (inherit tetrinet))
