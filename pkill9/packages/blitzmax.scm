;; Problem: blitzmax tries to create $(store-path)/tmp when compiling gridwars, and fails because it's read-only filesystem
(define-module (pkill9 packages blitzmax)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system trivial)
  #:use-module (guix licenses)
 )

; Example arguments to use:
;    (arguments
;     `(#:tests? #f
;       #:make-flags '("CC=gcc" "RM=rm" "SHELL=sh" "all")
;       #:phases (modify-phases %standard-phases (delete 'configure))
;       ))

(define-public blitzmax
(package
  (name "blitzmax")
  (version "0.98.3.27")
  ;(source
  ;  "/tmp/blitzmax-fixed.tar.xz")
  (source
    (origin
      (method url-fetch)
      (uri (string-append
             "https://github.com/bmx-ng/bmx-ng/releases/download/v"
             version
             ".linux.x64/BlitzMax_linux_x64_"
             version
             ".tar.xz"))
      (sha256
        (base32
          "15c5chvyj7r6z0565v3p1gm41vjazqsma9vlwp5w17v4naqq933q"))))
  (build-system trivial-build-system)
  (native-inputs
    `(("tar" ,(@ (gnu packages base) tar))
      ("bash" ,(@ (gnu packages bash) bash))
      ("xz" ,(@ (gnu packages compression) xz))
      ("patchelf" ,(@ (gnu packages elf) patchelf))))
  (inputs
    `(("glibc" ,(@ (gnu packages base) glibc))))
  (propagated-inputs
    `(("grep" ,(@ (gnu packages base) grep))
      ("coreutils" ,(@ (gnu packages base) coreutils))))
  (arguments
    `(#:modules ((guix build utils))
      #: builder (begin
      	     (use-modules (guix build utils))
                   (let ((out (assoc-ref %outputs "out"))
                         (source (assoc-ref %build-inputs "source"))
                         (bash (string-append (assoc-ref %build-inputs "bash") "/bin/bash"))
                         (xz (string-append (assoc-ref %build-inputs "xz") "/bin/xz"))
                         (tar (string-append (assoc-ref %build-inputs "tar") "/bin/tar"))
			 (patchelf (string-append (assoc-ref %build-inputs "patchelf") "/bin/patchelf")))
                           (mkdir out)
                           ;; We don't check for zero status because tar returns error  despite what we need getting extracted, i assume it's a badly built archive. For future imprvement: exclude extracting these files and check for zero status.
                           (system* bash "-c" 
                             (string-append xz
                                            " --decompress --stdout "
                                            source
                                            " | "
                                            tar " xvf -"))
			   ;we use system* instead of invoke because patchelf returns error code 1 for some reason
			   (invoke patchelf
				   "--set-interpreter"
				   (string-append (assoc-ref %build-inputs "glibc") "/lib/ld-linux-x86-64.so.2")
                                   "BlitzMax/bin/bmk")
			   (invoke patchelf
				   "--set-interpreter"
				   (string-append (assoc-ref %build-inputs "glibc") "/lib/ld-linux-x86-64.so.2")
                                   "BlitzMax/bin/bcc")
			   (invoke patchelf
				   "--set-interpreter"
				   (string-append (assoc-ref %build-inputs "glibc") "/lib/ld-linux-x86-64.so.2")
                                   "BlitzMax/bin/makedocs")
			   (invoke patchelf
				   "--set-interpreter"
				   (string-append (assoc-ref %build-inputs "glibc") "/lib/ld-linux-x86-64.so.2")
                                   "BlitzMax/bin/docmods")
                           (copy-recursively "BlitzMax" out)
                           )))) 
  (home-page "")
  (synopsis "")
  (description "")
  (license #f))

 )
(package (inherit blitzmax))

