(define-module (pkill9 packages gtetrinet)
  #:use-module (gnu packages)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gtk)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses)
  #:use-module (srfi srfi-1)
 )

; Example arguments to use:
;    (arguments
;     `(#:tests? #f
;       #:make-flags '("CC=gcc" "RM=rm" "SHELL=sh" "all")
;       #:phases (modify-phases %standard-phases (delete 'configure))
;       ))

(define-public gconf-gtk2
  (package (inherit gconf)
    (name "gconf-gtk2")
    (inputs
      `(,@(package-inputs gconf)
	 ("gtk+" ,gtk+-2)))
    (arguments
      `(,@(substitute-keyword-arguments (package-arguments gconf)
					((#:configure-flags cf)
					 `(cons "--with-gtk=2.0" ,cf)))))
    ))

(define-public gtetrinet
(package
  (name "gtetrinet")
  (version "0")
  (source
    (origin
      (method url-fetch)
      (uri "https://data.libregamenight.xyz/pub/tetrinet/gtetrinet/src.tar.gz")
      (sha256
        (base32
          "1cd2x14p4pvdw513qbwwvpawr2l1f2g9vrj91l9brc5q2igd027s"))))
  (build-system gnu-build-system)
  (arguments
   `(#:tests? #f ;no tests
     #:make-flags '("GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1")
     #:phases (modify-phases %standard-phases
     			     (add-after 'unpack 'fix-install-dir
     				(lambda _
     				  ;(mkdir-p (string-append (assoc-ref %outputs "out") "/bin"))
     				  (substitute* "src/Makefile.in"
                                         (("gamesdir = .*") "gamesdir = $(prefix)/bin\n"))
     				  #t)))
     ))
     
  (native-inputs
    `(("perl" ,(@ (gnu packages perl) perl))
      ("intltool" ,(@ (gnu packages glib) intltool))
      ("perl-xml-parser"
       ,(@ (gnu packages xml) perl-xml-parser))
      ("pkg-config"
       ,(@ (gnu packages pkg-config) %pkg-config))
      ("gettext"
       ,(@ (gnu packages gettext) gnu-gettext))
      ("sed" ,(@ (gnu packages base) sed))))
  (inputs
    `(("gtk+" ,(@ (gnu packages gtk) gtk+-2))
      ("libgnome" ,(@ (gnu packages gnome) libgnome))
      ("libgnomeui"
       ,(@ (gnu packages gnome) libgnomeui))))
  (propagated-inputs
    `(("gconf" ,(@ (gnu packages gnome) gconf))))
  (home-page "")
  (synopsis "")
  (description "")
  (license gpl3+))

 )
(package (inherit gtetrinet))
