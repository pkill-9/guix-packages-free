(define-module (pkill9 packages xfce4-taskbar-plugin gtkhotkey)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses)
 )

; Example arguments to use:
;    (arguments
;     `(#:tests? #f
;       #:make-flags '("CC=gcc" "RM=rm" "SHELL=sh" "all")
;       #:phases (modify-phases %standard-phases (delete 'configure))
;       ))

(define-public gtkhotkey
(package
  (name "gtkhotkey")
  (version "0.2.1")
  (source
    (origin
      (method url-fetch)
      (uri (string-append
             "https://launchpad.net/gtkhotkey/0.2/"
             version
             "/+download/gtkhotkey-"
             version
             ".tar.gz"))
      (sha256
        (base32
          "0rqx27jw2i2121s4lz33zsar1zxxdzsmc0qpavl93mr663cz077y"))))
  (build-system gnu-build-system)
  (native-inputs
    `(("sed" ,(@ (gnu packages base) sed))
      ("grep" ,(@ (gnu packages base) grep))
      ("gawk" ,(@ (gnu packages gawk) gawk))
      ("bzip2" ,(@ (gnu packages compression) bzip2))
      ("pkg-config"
       ,(@ (gnu packages pkg-config) %pkg-config))
      ("intltool" ,(@ (gnu packages glib) intltool))))
  (inputs
    `(("glib" ,(@ (gnu packages glib) glib))
      ("gobject-introspection"
       ,(@ (gnu packages glib) gobject-introspection))
      ("gtk+" ,(@ (gnu packages gtk) gtk+-2))))
  (arguments
    ;; fix compilation errors using this patch https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=gtkhotkey#n26
   `(#:tests? #f
     #:phases (modify-phases %standard-phases
			     (add-after 'unpack 'glib2-fix
				(lambda _
				  (substitute* "src/gtk-hotkey-error.h"
					(("#include <glib/gquark.h>")
					 "#include <glib.h>"))
				  (substitute* "src/x11/tomboykeybinder.h"
					(("#include <glib/gtypes.h>")
					 "#include <glib.h>"))
				    )))))
  (home-page "")
  (synopsis "")
  (description "")
  (license gpl3+))

 )
