(define-module (pkill9 packages sqlitebrowser)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages))

(define-public sqlitebrowser
  (package
   (name "sqlitebrowser")
   (version "git")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/sqlitebrowser/sqlitebrowser")
                  (commit "21d8800ce613775c995212f61d7e95c2189f6da6")))
            (file-name (git-file-name name version))
            (sha256
             (base32
              "0l811xz5df97cv88k45h9p3prnqc69m05v5w7qbvh9mnfvi54mla"))))
   (build-system cmake-build-system)
   (arguments
    `(#:tests? #f)) ;; No tests.
   (inputs
    `(("qtbase" ,(@ (gnu packages qt) qtbase))
      ("qttools" ,(@ (gnu packages qt) qttools))
      ("sqlite" ,(@ (gnu packages sqlite) sqlite))))
   (home-page "https://sqlitebrowser.org/")
   (synopsis "Graphical browser for sqlite databases")
   (description "DB Browser for SQLite is a high quality, visual, open source tool to create, design, and edit database files compatible with SQLite.

It is for users and developers wanting to create databases, search, and edit data. It uses a familiar spreadsheet-like interface, and you don't need to learn complicated SQL commands.")
   (license #f)))

(package (inherit sqlitebrowser))
