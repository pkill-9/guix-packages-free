;;Todo: Fix tests.

(define-module (pkill9 packages athenaeum)
  #:use-module (guix git-download)
  #:use-module (guix build qt-utils)
  #:use-module (guix build utils)
  #:use-module (guix build-system python)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages time)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages qt))

(define package-version-local package-version)

(define-public athenaeum
  (package
   (name "athenaeum")
   (version "1.2.0")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://gitlab.com/librebob/athenaeum.git")
                  (commit (string-append "v" version))))
            (file-name (git-file-name name version))
            (snippet ;; Fails to get version from git repository, so disable this
             '(begin
                (use-modules (guix build utils))
                (substitute* "setup.py"
                             (("use_scm_version=True") "use_scm_version=False"))))
            (sha256
             (base32
              "0ss8gjk46aaqlndaxs89zfcnfp40znzvp45v1kc5gr2fylywcf0m"))))
   (build-system python-build-system)
   (arguments
    `(#:tests? #f ;; Tests fail even though the application runs fine.
      #:modules ((guix build python-build-system) ;; Required for wrap-qt-program.
                  (guix build qt-utils)
                  (guix build utils))
      #:imported-modules (,@%python-build-system-modules ;; Required for wrap-qt-program.
                          (guix build qt-utils))
      #:phases (modify-phases %standard-phases
                              (add-after 'install 'wrap-qt
                               (lambda* (#:key outputs #:allow-other-keys)
                                 (wrap-qt-program (assoc-ref outputs "out") "athenaeum")
                                 #t))
                              (add-after 'install 'install-qml-files
                               (lambda* (#:key inputs outputs #:allow-other-keys)
                                 (let ((out (assoc-ref outputs "out"))
                                       (python-lib-path "lib/python3.8"))
                                   (for-each (lambda (file)
                                               (install-file file (string-append out "/" python-lib-path "/site-packages/athenaeum")))
                                             '("athenaeum/Athenaeum.qml"
                                               "athenaeum/BrowseView.qml"
                                               "athenaeum/FullscreenPreview.qml"
                                               "athenaeum/GameView.qml"
                                               "athenaeum/LibraryGridView.qml"
                                               "athenaeum/LibraryListView.qml"
                                               "athenaeum/LibraryView.qml"
                                               "athenaeum/NavigationBar.qml"
                                               "athenaeum/SearchView.qml"
                                               "athenaeum/SettingsView.qml")))
                                 #t))
                              (add-after 'install 'install-assets
                               (lambda* (#:key outputs #:allow-other-keys)
                                 (let ((out (assoc-ref outputs "out"))
                                       (python-lib-path "lib/python3.8"))
                                   (copy-recursively "athenaeum/icons" (string-append out "/" python-lib-path "/site-packages/athenaeum/icons"))
                                   #t)))
                              (add-after 'install 'install-desktop-icon
                               (lambda* (#:key outputs #:allow-other-keys)
                                 (copy-recursively "athenaeum/resources/icons"
                                                   (string-append (assoc-ref outputs "out") "/share/icons"))
                                 #t))
                              (add-after 'install 'install-desktop-file
                               (lambda* (#:key outputs #:allow-other-keys)
                                 (install-file "athenaeum/resources/com.gitlab.librebob.Athenaeum.desktop"
                                               (string-append (assoc-ref outputs "out") "/share/applications"))
                                 #t)))))
   (native-inputs
    `(("python-setuptools-scm" ,python-setuptools-scm)))
   (inputs
    `(("python-pyqt" ,python-pyqt)
      ("python-dateutil" ,python-dateutil)
      ("python-numpy" ,python-numpy)
      ("qtsvg" ,qtsvg)
      ("qtquickcontrols2" ,qtquickcontrols2)
      ("qtdeclarative" ,qtdeclarative)
      ("qtgraphicaleffects" ,qtgraphicaleffects)))
   (home-page "https://gitlab.com/librebob/athenaeum")
   (synopsis "Launcher for libre games")
   (description "A browser and launcher for libre games.  Uses Flatpak for getting games.")
   (license license:gpl3)))

athenaeum
