(define-module (pkill9 packages love)
  #:use-module (guix gexp) ;; plain-file
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (pkill9 guix build-system love-wrapper)
  #:use-module (gnu packages game-development)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages image)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages mp3)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages pkg-config)
  #:use-module (srfi srfi-1)
  )

(define patch-love-0.7.2
  ;; Fix love-0.7.2 build: https://aur.archlinux.org/packages/love07/#comment-684696
  (plain-file "love-0.7.2-patch.txt"
   "diff --git a/src/modules/graphics/opengl/Framebuffer.cpp b/src/modules/graphics/opengl/Framebuffer.cpp
index 93575d7..ea36807 100644
--- a/src/modules/graphics/opengl/Framebuffer.cpp
+++ b/src/modules/graphics/opengl/Framebuffer.cpp
@@ -1,3 +1,5 @@
+#define GL_GLEXT_PROTOTYPES
+
 #include \"Framebuffer.h\"
 #include <common/Matrix.h>

diff --git a/src/modules/graphics/opengl/SpriteBatch.cpp b/src/modules/graphics/opengl/SpriteBatch.cpp
index ca5762f..8d6dcda 100644
--- a/src/modules/graphics/opengl/SpriteBatch.cpp
+++ b/src/modules/graphics/opengl/SpriteBatch.cpp
@@ -1,3 +1,5 @@
+#define GL_GLEXT_PROTOTYPES
+
 /**
 * Copyright (c) 2006-2011 LOVE Development Team
 * "))

(define-public love-0.7.2
  (package (inherit love)
    (version "0.7.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://github.com/love2d/love/releases/download/0.7.2/love-0.7.2-linux-src.tar.gz")
       (sha256
        (base32 "0s7jywkvydlshlgy11ilzngrnybmq5xlgzp2v2dhlffwrfqdqym5"))
       (patches (list patch-love-0.7.2))))
    (native-inputs
     `(("pkg-config" ,pkg-config)))
    (inputs
     `(("devil" ,devil)
       ("freetype" ,freetype)
       ("libmodplug" ,libmodplug)
       ("libtheora" ,libtheora)
       ("libvorbis" ,libvorbis)
       ("mesa" ,mesa)
       ("mpg123" ,mpg123)
       ("openal" ,openal)
       ("physfs" ,physfs)
       ("zlib" ,zlib)
       ("lua" ,lua-5.1)
       ("sdl" ,sdl)
       ("libmng" ,libmng)
       ("libtiff" ,libtiff)
       ("glu" ,glu)
       ("freetype" ,freetype)))
     (arguments
     '(#:configure-flags
       (list (string-append "CPPFLAGS=-I"
                            (assoc-ref %build-inputs "sdl")
                            "/include/SDL"
			    " -I"
			    (assoc-ref %build-inputs "freetype")
			    "/include/freetype2"))))))

(define-public love-0.8.0
  (package (inherit love-0.7.2)
    (version "0.8.0")
    (source
     (origin
       (method url-fetch)
       (uri "https://github.com/love2d/love/releases/download/0.8.0/love-0.8.0-linux-src.tar.gz")
       (sha256
        (base32 "1k4fcsa8zzi04ja179bmj24hvqcbm3icfvrvrzyz2gw9qwfclrwi"))))))

(define-public luajit-2.0.4
  (package
    (name "luajit")
    (version "2.0.4")
    (source (origin
              (method url-fetch)
              (uri (string-append "http://luajit.org/download/LuaJIT-"
                                  version ".tar.gz"))
              (sha256
               (base32 "0zc0y7p6nx1c0pp4nhgbdgjljpfxsb5kgwp4ysz22l1p2bms83v2"))
              (patches (search-patches "luajit-2.0.4-symlinks.patch"
                                       "luajit-2.0.4-no_ldconfig.patch"))))
    (build-system gnu-build-system)
    (arguments
     '(#:tests? #f                      ;luajit is distributed without tests
       #:phases (alist-delete 'configure %standard-phases)
       #:make-flags (list (string-append "PREFIX=" (assoc-ref %outputs "out")))))
    (home-page "http://www.luajit.org/")
    (synopsis "Just in time compiler for Lua programming language version 5.1")
    (description
     "LuaJIT is a Just-In-Time Compiler (JIT) for the Lua
programming language.  Lua is a powerful, dynamic and light-weight programming
language.  It may be embedded or used as a general-purpose, stand-alone
language.")
;;    (license license:x11)))
    (license #f)))

(define-public love-0.10.2
  (package (inherit love)
    (version "0.10.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://github.com/love2d/love/releases/download/0.10.2/love-0.10.2-linux-src.tar.gz")
       (sha256
        (base32 "11x346pw0gqad8nmkmywzx4xpcbfc3dslbrdw5x94n1i25mk0sxj"))))
    (inputs
     `(,@(alist-delete "luajit" (package-inputs love))
       ("luajit" ,luajit-2.0.4)
       ("physfs" ,physfs)))))

(define-public not-tetris
  (package
   (name "not-tetris-2")
   (version "0")
   (source (origin
            (method url-fetch)
            (uri "http://stabyourself.net/dl.php?file=nottetris2/nottetris2-linux.zip")
            (sha256
             (base32
              "13585qcmds9yx8r1wnigd0mp6n3m5cx41sndn2xp84bn3rz77f72"))))
   (build-system love-wrapper-build-system)
   (arguments
    `(#:love , love-0.7.2))
   (home-page "http://stabyourself.net/nottetris2")
   (synopsis "Tetris with physics")
   (description synopsis)
   (license #f)))

(define-public tripong
  (package
   (name "tripong")
   (version "1.0.2")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/AlexanDDOS"
                                "tripong/releases/download/v" version "/"
                                "tripong.love"))
            (sha256
             (base32
              "1ww7bgdvgdh9vg17vk7j06nbicpclr1fqfn46w91pvg0xvq6wjn8"))))
   (build-system love-wrapper-build-system)
   (home-page "https://github.com/AlexanDDOS/tripong")
   (synopsis "Pong in a triangle")
   (description synopsis)
   (license #f)))

(define-public creature-synth
  (package
   (name "creature-synth")
   (version "0")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://cdn.discordapp.com"
                                "/attachments/474705430434807819/605901876864614431/"
                                "creature-synth.love"))
            (sha256
             (base32
              "05007316z35bcrnsl3j0y1vc14z9njlh1m9fclr2jrwn17igj3kc"))))
   (build-system love-wrapper-build-system)
   (arguments
    `(#:category "Audio"))
   (home-page #f)
   (synopsis "Simulate a vocal tract")
   (description synopsis)
   (license #f)))

(define-public mario-kart-extreme-drift
  (package
   (name "mario-kart-extreme-drift")
   (version "0.2")
   (source (origin
            (method url-fetch)
            (uri "https://love2d.org/forums/download/file.php?id=17143")
            (file-name (string-append name "-" version ".love"))
            (sha256
             (base32
              "0pbl4dq7524nvh52y5xc5ag0kdv4qhvzrygk9cmdaf1dm7nfajnw"))))
   (build-system love-wrapper-build-system)
   (home-page "https://love2d.org/forums/viewtopic.php?f=14&t=86346")
   (synopsis "Mario Kart Clone")
   (description "synopsis")
   (license #f)))

(define-public bab-be-you
  (package
   (name "bab-be-you")
   (version "1.7.0")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/lilybeevee/bab-be-u.git")
                  (commit (string-append "v" version))))
            (file-name (git-file-name name version))
            (sha256
             (base32
              "0i6nrcwq7idzfcm298z6mqv2x7w48cba9051j49g5fdhc3ngmxd6"))))
   (build-system love-wrapper-build-system)
   (home-page "https://github.com/lilybeevee/bab-be-u")
   (synopsis "Cheap clone of Baba Is You")
   (description synopsis)
   (license #f)))

(define-public bytepath
  (package
   (name "bytepath")
   (version "git")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/a327ex/BYTEPATH.git")
                  (commit "73fc96a8b5036897f024afa89795387718558ad5")))
            (file-name (git-file-name name version))
            (snippet
             '(begin
                (use-modules (guix build utils))
                (substitute* "main.lua"
                 (("Steam = require 'libraries/steamworks'") "Steam = nil"))))
            (sha256
             (base32
              "1nfp165fnsrx6lqnhlxmy8hswik9wcy619bgj20z044sq7grba52"))))
   (build-system love-wrapper-build-system)
   (arguments
    `(#:love ,love-0.10.2))
   (home-page "https://github.com/a327ex/BYTEPATH")
   (synopsis "2D space shooter")
   (description synopsis)
   (license license:expat)))

love-0.10.2
