(define-module (pkill9 packages python-padatious)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system python)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages swig))

(define-public fann
  (let ((commit "7ec1fc7e5bd734f1d3c89b095e630e83c86b9be1")
        (revision "1"))
    (package
     (name "fann")
     (version (git-version "2.2.0" revision commit))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/libfann/fann")
                    (commit commit)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1csxjaq8vw05dnrb4i2q9rnnkksv5z6jdkwzpgj9xwwgfq2yyzyc"))))
     (build-system cmake-build-system)
     (arguments
      `(#:tests? #f)) ;; no tests
     (home-page "https://github.com/libfann/fann")
     (synopsis "Fast Artificail Neural Network Library (FANN)")
     (description "Fast Artificial Neural Network (FANN) Library is a free open source neural network library, which implements multilayer artificial neural networks in C with support for both fully connected and sparsely connected networks.")
     (license license:lgpl2.1))))

(define-public python-padaos
  (package
    (name "python-padaos")
    (version "0.1.10")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "padaos" version))
        (sha256
          (base32
            "0wkd6p3ggf3ffsg3j47fgfcfmmj5k7h5rak88mbkr1r6r35mzh1a"))))
    (build-system python-build-system)
    (home-page
      "http://github.com/MatthewScholefield/padaos")
    (synopsis
      "A rigid, lightweight, dead-simple intent parser")
    (description
      "A rigid, lightweight, dead-simple intent parser")
    (license license:expat)))

(define-public python-xxhash
  (package
    (name "python-xxhash")
    (version "1.4.3")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "xxhash" version))
        (sha256
          (base32
            "1sax8fd927zqmdl2rxvlq7jnnpmv3s0lm2rrn35xkmrifzz1lswb"))))
    (build-system python-build-system)
    (home-page
      "https://github.com/ifduyue/python-xxhash")
    (synopsis "Python binding for xxHash")
    (description "Python binding for xxHash")
    (license license:bsd-3)))

(define-public python-fann2
  (package
    (name "python-fann2")
    (version "1.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "fann2" version))
        (sha256
          (base32
            "07nlpncl5cx2kzdy3r91g3i1bsnl7n6f7zracwh87q28mmjhmjnd"))))
    (build-system python-build-system)
    (inputs
     `(("fann" ,fann)
       ("swig" ,swig)))
    (arguments
     `(#:phases (modify-phases %standard-phases
                               (add-after 'unpack 'patch-fann-source-lib-path
                                          (lambda* (#:key inputs #:allow-other-keys)
                                            (let* ((fann (assoc-ref inputs "fann")))
                                              (substitute* "setup.py"
                                                           (("dirs = .*") (string-append "dirs = ['" fann "/lib']\n") )))
                                            #t))
                               (add-after 'unpack 'patch-swig-bin-path
                                          (lambda* (#:key inputs #:allow-other-keys)
                                            (let* ((swig (assoc-ref inputs "swig"))
                                                   (swigbinpath (string-append swig "/bin/swig")))
                                              (substitute* "setup.py"
                                                           (("swig_bin = .*") (string-append "swig_bin = '" swigbinpath "'\n") )))
                                            #t))
                               )))
    (home-page
      "https://github.com/FutureLinkCorporation/fann2")
    (synopsis
      "Fast Artificial Neural Network Library (FANN) Python bindings.")
    (description
      "Fast Artificial Neural Network Library (FANN) Python bindings.")
    (license #f)))

(define-public python-padatious
  (package
    (name "python-padatious")
    (version "0.4.7")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "padatious" version))
        (sha256
          (base32
            "07xcz8s9pa40zb926rq4jxydbm56v1mpgz9ncp1s6la1zx15j8rw"))))
    (build-system python-build-system)
    (propagated-inputs
      `(("python-fann2" ,python-fann2)
        ("python-padaos" ,python-padaos)
        ("python-xxhash" ,python-xxhash)))
    (home-page
      "http://github.com/MycroftAI/padatious")
    (synopsis "A neural network intent parser")
    (description "A neural network intent parser")
    (license #f)))

python-padatious
