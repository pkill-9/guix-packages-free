;; ALL CREDITS GO TO leo-lb
;; All this code is from their post at https://github.com/skyjake/lagrange/issues/225

(define-module (pkill9 packages lagrange)
 #:use-module ((guix licenses) #:prefix license:)
 #:use-module (gnu packages sdl)
 #:use-module (gnu packages curl)
 #:use-module (gnu packages compression)
 #:use-module (gnu packages pcre)
 #:use-module (gnu packages mp3)
 #:use-module (gnu packages tls)
 #:use-module (gnu packages libunistring)
 #:use-module (gnu packages version-control)
 #:use-module (guix packages)
 #:use-module (guix build-system cmake)
 #:use-module (gnu packages)
 #:use-module (guix git-download)
 #:use-module (gnu packages pkg-config))

(define the-foundation
  (let ((commit "8172d35cad24ec392bfd27e1f4de3a2205f988d2"))
    (package
     (name "the-foundation")
     (version (string-take commit 7))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.skyjake.fi/skyjake/the_Foundation")
                    (commit commit)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "010kgbii33rb52g6iyrcjpb9sicnfw684pyrfwv1h8igpiv6hsv9"))))
     (build-system cmake-build-system)
     (arguments
      `(;; Tests are not meant to be run automatically for now.
        ;; See https://codeberg.org/skyjake/the_Foundation/issues/2
        #:tests? #f))
     (native-inputs
      `(("git" ,git-minimal)
        ("pkg-config" ,pkg-config)))
     (inputs
      `(("curl" ,curl)
        ("zlib" ,zlib)
        ("pcre" ,pcre)
        ("openssl" ,openssl)
        ("libunistring" ,libunistring)))
     (home-page "https://git.skyjake.fi/skyjake/the_Foundation")
     (synopsis "Opinionated C11 library for low-level functionality")
           (description "@code{the_Foundation} is a C11 library and a coding
convention for object-oriented programming that has been designed from the
point of view of someone who appreciates the user-friendliness of Qt and some
of the thinking behind C++ STL.")
           (license license:bsd-2))))

;; When upgrading, also upgrade the "the-foundation" package just above.
;; Lagrange pins specific commits of it and "the-foundation" does not make
;; releases (yet?).
(define-public lagrange
  (package
   (name "lagrange")
   (version "1.2.3")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://git.skyjake.fi/skyjake/lagrange")
                  (commit (string-append "v" version))))
            (file-name (git-file-name name version))
            (sha256
             (base32
              "0djwr0n06z993kdpvks1dj7nr4j6481sfh5mjjg7q2hsfnsj9kmx"))
            (modules '((guix build utils)))
            (snippet
             '(begin
                (delete-file-recursively "lib/the_Foundation")
                #t))))
   (build-system cmake-build-system)
   (arguments
    `(;; No tests
      #:tests? #f))
   (native-inputs
    `(("pkg-config" ,pkg-config)))
   (inputs
    `(("the-foundation" ,the-foundation)
      ("sdl2" ,sdl2)
      ("curl" ,curl)
      ("zlib" ,zlib)
      ("pcre" ,pcre)
      ("mpg123" ,mpg123)))
   (home-page "https://gmi.skyjake.fi/lagrange/")
   (synopsis "Beautiful desktop GUI client for browsing Geminispace")
       (description "@code{Lagrange} is a desktop GUI client for browsing
Geminispace.  It offers modern conveniences familiar from web browsers, such
as smooth scrolling, inline image viewing, multiple tabs, visual themes,
Unicode fonts, bookmarks, history, and page outlines.")
       (license license:bsd-2)))

lagrange
