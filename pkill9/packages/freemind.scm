;FIXME: fix tests

(define-module (pkill9 packages freemind)
  #:use-module (guix download)
  #:use-module (guix build-system ant)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages))

(define-public freemind
  (package
   (name "freemind")
   (version "1.0.1")
   (source (origin
	    (method url-fetch)
	    (uri (string-append "mirror://sourceforge/freemind/freemind-src-" version ".tar.gz"))
	    
	    (sha256
	     (base32
	      "06c6pm7hpwh9hbmyah3lj2wp1g957x8znfwc5cwygsi7dc98b0h1"))))
   (build-system ant-build-system)
   (inputs
    `(("icedtea" ,(@ (gnu packages java) icedtea))))
   (arguments
    `(#:tests? #f ; FIXME:There are tests, but no test target.
      #:build-target "dist"
      #:phases (modify-phases %standard-phases
			      (delete 'generate-jar-indices) ; hack to get build to complete
			      (add-before 'configure 'dunno-if-this-is-needed ; used in nix pkg definition
			       (lambda _
				 (setenv "JAVA_TOOL_OPTIONS" "-Dfile.encoding=UTF8")))
                              (add-after 'unpack 'allow-exec-permission
					 (lambda _
					   (chmod "check_for_duplicate_resources.sh" #o755)))
			      (replace 'install
				       (lambda _
					 (mkdir (assoc-ref %outputs "out"))
					 (mkdir (string-append (assoc-ref %outputs "out") "/opt"))
					 (mkdir (string-append (assoc-ref %outputs "out") "/bin"))
					 (copy-recursively "../bin/dist"
							   (string-append (assoc-ref %outputs "out") "/opt/freemind"))
					 (symlink (string-append (assoc-ref %outputs "out") "/opt/freemind/freemind.sh")
						  (string-append (assoc-ref %outputs "out") "/bin/freemind"))
					 (chmod (string-append (assoc-ref %outputs "out") "/bin/freemind") #o755)
					 (wrap-program (string-append (assoc-ref %outputs "out") "/bin/freemind")
						       `("JAVA_BINDIR" = (,(string-append (assoc-ref %build-inputs "icedtea") "/bin")))))))))
   (home-page "https://freemind.sourceforge.net")
   (synopsis "A mind-mapping application written in Java.")
   (description "A mind-mapping application written in Java.")
   (license license:gpl2)))

(package (inherit freemind))
