(define-module (pkill9 packages pmbootstrap)
  #:use-module (guix git-download)
  #:use-module (guix build-system python)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages))

(define-public pmbootstrap
  (package
    (name "pmbootstrap")
    (version "1.11.0")
    (source
     (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://gitlab.com/postmarketOS/pmbootstrap.git")
            (commit version)))
      (file-name (git-file-name name version))
      (sha256
       (base32
        "1n45icvw2a17zmqyqyqk80h2nyr3ahxqwmqhyfd4900f4nyb02kv"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f)) ;; Test phase tries to download missing dependencies required to run tests. FIXME: Add this dependency/dependencies and enable tests.
    (home-page "https://www.postmarketos.org")
    (synopsis
      "A sophisticated chroot / build / flash tool to develop and install postmarketOS")
    (description
      "A sophisticated chroot / build / flash tool to develop and install postmarketOS")
    (license #f)))

pmbootstrap
