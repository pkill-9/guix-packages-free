(define-module (pkill9 packages perl-graph-easy)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (gnu packages perl)
  #:use-module (guix build-system perl)
  #:use-module ((guix licenses) #:prefix license:))

(define-public perl-graph-easy
  (package 
    (name "perl-graph-easy")
    (version "0.76")
    (source
      (origin
        (method url-fetch)
        (uri (string-append
               "mirror://cpan/authors/id/S/SH/SHLOMIF/Graph-Easy-"
               version
               ".tar.gz"))
        (sha256
          (base32
            "1yni1181bqfvqcr155mvzgqsqlmpwfiklzx3ircknrpgxc5c38nl"))))
    (build-system perl-build-system)
    (native-inputs
      `(("perl-module-build" ,perl-module-build)))
    (home-page
      "https://metacpan.org/release/Graph-Easy")
    (synopsis
      "Convert or render graphs (as ASCII, HTML, SVG or via Graphviz)")
    (description synopsis)
    (license license:gpl1)))

perl-graph-easy
