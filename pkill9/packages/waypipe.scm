(define-module (pkill9 packages waypipe)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system meson)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages video)
  #:use-module (gnu packages man)
  #:use-module (gnu packages pkg-config))

(define-public waypipe
  (package
    (name "waypipe")
    (version "0.7.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gitlab.freedesktop.org/mstoeckl/waypipe/")
               (recursive? #t)
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "19pxym3phxdrjb6c2xkkbz3m2a8kzsgwipgplybl72hr9i4fpmrf"))))
    (build-system meson-build-system)
    (native-inputs
     `(("pkg-config" ,pkg-config)
       ("scdoc" ,scdoc)))
    (inputs
     `(("lz4" ,lz4)
       ("zstd" ,zstd "lib")
       ("libdrm" ,libdrm)
       ("ffmpeg" ,ffmpeg)
       ("libva" ,libva)))
    (arguments
     `(#:tests? #f ; TODO: Setup tests which require environment variables set and extra inputs
     ))
    (home-page "https://gitlab.freedesktop.org/mstoeckl/waypipe/")
    (synopsis "Network transparency with Wayland")
    (description
      "Waypipe is a proxy for Wayland clients.  It forwards Wayland messages and serializes changes to shared memory buffers over a single socket.  This makes application forwarding similar to ssh -X feasible.")
    (license "expat")))

waypipe
