(define-module (pkill9 packages goldberg-emulator)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages))

(define patch-use-g++
  (plain-file "goldberg-emulator-0.2.3-use-g++.patch"
   "diff --git a/Makefile b/Makefile
index eb3f9e6..d4747b5 100644
--- a/Makefile
+++ b/Makefile
@@ -1,6 +1,6 @@
 .DEFAULT_GOAL := all
 
-CXX=clang++
+CXX=g++
 CXX_FLAGS += -fPIC -std=c++11
 LD_FLAGS += -shared -lprotobuf-lite -Wl,--no-undefined
 LIBRARY_NAME=libsteam_api.so
"))

(define-public goldberg-emulator
  (package
   (name "goldberg-emulator")
   (version "0.2.3")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://gitlab.com/Mr_Goldberg/goldberg_emulator")
                  (commit (string-append "v" version))))
            (file-name (git-file-name name version))
            (patches (list patch-use-g++))
            (sha256
             (base32
              "14azaqkqlrzxbrnqrc58njfrf6d8xkhw109d147i0sk42aka3mx1"))))
   (build-system gnu-build-system)
   (arguments
    `(#:tests? #f ;; No tests
      #:phases
      (modify-phases
       %standard-phases
       (delete 'configure) ;; No configure file
       (replace 'install ;; No install target
                (lambda* (#:key outputs #:allow-other-keys)
                  (let* ((out (assoc-ref %outputs "out")))
                    (mkdir out)
                    (install-file "libsteam_api.so" (string-append out "/lib"))))))))
   (inputs
    `(("protobuf" ,(@ (gnu packages protobuf) protobuf))))
   (home-page "https://mr_goldberg.gitlab.io/goldberg_emulator")
   (synopsis "Steam emulator")
   (description "Steam emulator for GNU/Linux and Windows that emulates steam online features. Lets you play games that use the steam multiplayer apis on a LAN without steam or an internet connection.
")
   (license license:lgpl3)))

goldberg-emulator
