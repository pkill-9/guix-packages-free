(define-module (pkill9 packages font-siji)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (guix utils)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (gnu packages base)
  #:use-module (guix build utils)
  #:use-module (guix build-system trivial))

(define-public font-siji
  (package
    (name "font-siji")
    (version "0")
    (source
      (origin
        (method git-fetch)
       (uri (git-reference
              (url "https://github.com/stark/siji")
              (commit "9d88311bb127b21672b2d4b43eed1ab0e494f143")))
        (sha256
          (base32
            "19vx18bgbavybccvyyjpscgpikvl9igs04n0vvx9xzm4d01k30si"))))
    (build-system trivial-build-system)
    (arguments
      `(#:modules ((guix build utils))
	#:builder (begin
		     (use-modules (guix build utils))
                     (let* ((output (assoc-ref %outputs "out"))
                           (source (assoc-ref %build-inputs "source")))
                           ; Todo: copy over license
			   (copy-recursively
			     (string-append source "/bdf")
			     (string-append output "/share/fonts/siji/bdf"))
			   (copy-recursively
			     (string-append source "/pcf")
			     (string-append output "/share/fonts/siji/pcf"))
			   ))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(package (inherit font-siji))
