(define-module (pkill9 packages casa)
  #:use-module (guix git-download)
  #:use-module (guix build-system meson)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages wm))

(define-public fdicons
  (let ((revision "1")
        (commit "3a808f5580d25f1a092341bcd003908b4f6f66fa"))
    (package
     (name "fdicons")
     (version (git-version "0" revision commit))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://gitlab.freedesktop.org/ddevault/fdicons")
                    (commit commit)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "17cpf24z28m0wjjmnbf8igb7xclqp0kpn7paqqlib7wyin9hsx1q"))))
     (build-system meson-build-system)
     (home-page "https://gitlab.freedesktop.org/ddevault/fdicons")
     (synopsis "Library for accessing the FreeDesktop icon theme database")
     (description "synopsis")
     (license license:expat))))

(define-public casa
  (let ((revision "1")
        (commit "019dcb8e05f2a02e767a09415ac56a55ba4b9c7e"))
    (package
     (name "casa")
     (version (git-version "0" revision commit))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.sr.ht/~sircmpwn/casa")
                    (commit commit)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1djiv7fkx5h37arcp85mkfcl86ajpm9js1ikxknk77xkpmvwzxds"))))
     (native-inputs (package-native-inputs sway)) ;; lazy
     (inputs
      `(,@(package-inputs sway) ;; lazy
        ("fdicons" ,fdicons)))
     (propagated-inputs (package-propagated-inputs sway)) ;; lazy
     (build-system meson-build-system)
     (arguments
      `(#:phases (modify-phases %standard-phases
                                (replace 'install
                                         (lambda* (#:key outputs #:allow-other-keys)
                                           (let ((out (assoc-ref outputs "out")))
                                             (install-file "casa"
                                                           (string-append out "/bin")))
                                           #t)))))
     (home-page "https://git.sr.ht/~sircmpwn/casa")
     (synopsis "Home screen for wayland compositors")
     (description "Home screen for wayland compositors, written for wlroots")
     (license license:gpl3))))

casa
