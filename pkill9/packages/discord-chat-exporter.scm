(define-module (pkill9 packages discord-chat-exporter)
  #:use-module (guix download)
  #:use-module (guix build-system trivial)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages))

(define-public discord-chat-exporter
  (package
   (name "discord-chat-exporter")
   (version "2.14")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/"
                                "Tyrrrz/DiscordChatExporter/releases/download/" version "/"
                                "DiscordChatExporter.CLI.zip"))
            
            (sha256
             (base32
              "11jh455z79fjvmj49zy856dbxwyla709x7dvs5ywvhczk73zqcnd"))))
   (build-system trivial-build-system)
   (native-inputs
    `(("unzip" ,(@ (gnu packages compression) unzip))))
   (inputs
    `(("bash" ,(@ (gnu packages bash) bash))
      ("mono" ,(@ (gnu packages mono) mono))
      ("ca-certificates-mono" ,(@ (pkill9 packages openra ca-certificates-mono) ca-certificates-mono))))
   (arguments
    `(#:modules ((guix build utils))
      #:builder
      (begin
        (use-modules (guix build utils))
        (let* ((source (assoc-ref %build-inputs "source"))
               (out (assoc-ref %outputs "out"))
               (bash (assoc-ref %build-inputs "bash"))
               (unzip (string-append (assoc-ref %build-inputs "unzip") "/bin/unzip"))
               (mono (assoc-ref %build-inputs "mono"))
               (certificates (assoc-ref %build-inputs "ca-certificates-mono")))
          (mkdir-p "opt/discord-chat-exporter")
          (mkdir "bin")
          (invoke unzip source "-d" "opt/discord-chat-exporter")
          (with-output-to-file "bin/discord-chat-exporter"
            (lambda _
              (format #t
                      "#!~a/bin/bash~@
export XDG_CONFIG_HOME=~a~@
~a/bin/mono ~a/opt/discord-chat-exporter/DiscordChatExporter.Cli.exe \"$@\""
                      bash certificates mono out)))
          (chmod "bin/discord-chat-exporter" #o755)
          (copy-recursively "." out)
        ))))
   (home-page "http://www.tyrrrz.me/Projects/DiscordChatExporter")
   (synopsis "Export Discord chat logs")
   (description synopsis)
   (license license:gpl3+)))

discord-chat-exporter
