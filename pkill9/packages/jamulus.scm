(define-module (pkill9 packages jamulus)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages audio))

(define-public jamulus
  (package
   (name "jamulus")
   (version "3.4.4")
   (source (origin
            (method url-fetch)
            (uri (string-append "mirror://sourceforge"
                                "/llcon/Jamulus/" version "/"
                                "Jamulus-" version ".tar.gz"))
            (sha256
             (base32
              "15vc9sbjgrhnc4lrpbfspdw6nx0sj7i8lbzg8l16lknnabs12rl8"))))
   (build-system gnu-build-system)
   (inputs
    `(("qtbase" ,qtbase)
      ("jack" ,jack-1)))
   (arguments
    `(#:phases (modify-phases %standard-phases
                              (replace 'configure 
                               (lambda* (#:key inputs #:allow-other-keys)
                                 (let ((qmake (string-append
                                               (assoc-ref inputs "qtbase")
                                               "/bin/qmake")))
                                   (invoke qmake "Jamulus.pro"))))
                              (replace 'install
                               (lambda* (#:key outputs #:allow-other-keys)
                                 (let ((out (assoc-ref outputs "out")))
                                   (install-file "Jamulus" (string-append out "/bin"))))))))
   (home-page "http://llcon.sourceforge.net")
   (synopsis "Real-time jam sessions over the internet")
   (description "The Jamulus software enables musicians to perform real-time jam sessions over the internet.  There is one server running the Jamulus server software which collects the audio data from each Jamulus client, mixes the audio data and sends the mix back to each client.")
   (license license:gpl2)))

jamulus
