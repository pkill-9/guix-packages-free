;;TODO: Unbundle things from src/downloader/lib

(define-module (pkill9 packages springlobby)
  #:use-module (guix gexp)
  #:use-module (guix download)
  #:use-module (guix build-system cmake)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages wxwidgets)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages image)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages tls))

(define patch-disable-building-bundled-jsoncpp ; Not a hack, but a sledgehammering. I just removed a couple chunks until it worked.
  (plain-file "disable-bundled-jsoncpp.patch"
              "diff --git a/src/downloader/lib/CMakeLists.txt b/src/downloader/lib/CMakeLists.txt
index 8b41a40..69f1c80 100644
--- a/src/downloader/lib/CMakeLists.txt
+++ b/src/downloader/lib/CMakeLists.txt
@@ -32,13 +32,6 @@ option(PRD_ARCHIVE_SUPPORT \"enable archive support\" ON)
 option(PRD_CLEAR_COMPILER_FLAGS \"clear all compiler flags\" OFF)
 option(PRD_DEFAULT_LOGGER \"use default logger\" ON)
 
-find_package(Jsoncpp)
-if (${Jsoncpp_FOUND})
-	option(PRD_JSONCPP_INTERNAL \"use bundled JsonCpp\" FALSE)
-else()
-	option(PRD_JSONCPP_INTERNAL \"use bundled JsonCpp\" TRUE)
-endif()
-
 if(PRD_CLEAR_COMPILER_FLAGS)
 	set(CMAKE_CXX_FLAGS \"\")
 	set(CMAKE_C_FLAGS \"\")
@@ -130,16 +123,9 @@ endif()
 #message(STATUS \"Using CURL_LD_FLAGS: ${CURL_LD_FLAGS}\")
 
 
-if (PRD_JSONCPP_INTERNAL)
-        # use bundled JsonCpp
-        set(PRD_JSONCPP_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src/lib/jsoncpp/include)
-        set(PRD_JSONCPP_LIBRARIES \"\")
-else()
-        find_package(Jsoncpp REQUIRED)
-	set(PRD_JSONCPP_INCLUDE_DIR ${Jsoncpp_INCLUDE_DIR})
-	set(PRD_JSONCPP_LIBRARIES ${Jsoncpp_LIBRARY})
-endif()
-
+find_package(Jsoncpp REQUIRED)
+set(PRD_JSONCPP_INCLUDE_DIR ${Jsoncpp_INCLUDE_DIR})
+set(PRD_JSONCPP_LIBRARIES ${Jsoncpp_LIBRARY})
 
 # MINIZIP_FOUND is used in lib & FileSystem
 find_package(MiniZip)
diff --git a/src/downloader/lib/src/CMakeLists.txt b/src/downloader/lib/src/CMakeLists.txt
index b7ed78c..2413ace 100644
--- a/src/downloader/lib/src/CMakeLists.txt
+++ b/src/downloader/lib/src/CMakeLists.txt
@@ -6,14 +6,6 @@ if(PRD_ARCHIVE_SUPPORT)
 	add_definitions(-DARCHIVE_SUPPORT)
 endif()
 
-if (PRD_JSONCPP_INTERNAL)
-	set(jsonlibcppsrc
-		${CMAKE_CURRENT_SOURCE_DIR}/lib/jsoncpp/src/lib_json/json_value.cpp
-		${CMAKE_CURRENT_SOURCE_DIR}/lib/jsoncpp/src/lib_json/json_reader.cpp
-		${CMAKE_CURRENT_SOURCE_DIR}/lib/jsoncpp/src/lib_json/json_writer.cpp
-	)
-endif()
-
 add_library(Downloader STATIC
 	Downloader/Rapid/RapidDownloader.cpp
 	Downloader/Rapid/Repo.cpp"))

(define patch-fix-compilation-errors-springlobby
  (plain-file "fix-compilation-errors-springlobby"
              "diff --git a/src/gui/hosting/battleroomdataviewmodel.cpp b/src/gui/hosting/battleroomdataviewmodel.cpp
index 7ce1505..917879a 100644
--- a/src/gui/hosting/battleroomdataviewmodel.cpp
+++ b/src/gui/hosting/battleroomdataviewmodel.cpp
@@ -131,7 +131,7 @@ void BattleroomDataViewModel::GetValue(wxVariant& variant,
 			if (user->GetTrueSkill() == 0 || isBridged) {
 				variant = wxString(_T(\"-\"));
 			} else {
-				variant = wxString::Format(_T(\"%ld\"), std::lround(user->GetTrueSkill()));
+				variant = wxString::Format(_T(\"%ld\"), lround(user->GetTrueSkill()));
 			}
 			break;
 
diff --git a/src/gui/mapctrl.cpp b/src/gui/mapctrl.cpp
index b5b195b..709c674 100644
--- a/src/gui/mapctrl.cpp
+++ b/src/gui/mapctrl.cpp
@@ -599,7 +599,7 @@ void MapCtrl::DrawStartRect(wxDC& dc, int index, wxRect& sr, const wxColour& col
 		const double metal = GetStartRectMetalFraction(index);
 		if (metal != 0.0) {
 			wxString strMetal = wxString::Format(_(\"Metal: %.1f%%\"),
-							     std::isnan(metal) ? 0.0 : metal * 100.0);
+							     __builtin_isnan(metal) ? 0.0 : metal * 100.0);
 			dc.GetTextExtent(strMetal, &twidth, &theight);
 			// don't cramp it in rect, but only display it if it actually fits
 			if (sr.height >= 6 * theight && sr.width > twidth) {"))

(define-public alure
  (package
   (name "alure")
   (version "1.2")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://" version ".tar.gz"))
            
            (sha256
             (base32
              "0w8gsyqki21s1qb2s5ac1kj08i6nc937c0rr08xbw9w9wvd6lpj6"))))
   (build-system cmake-build-system)
   (inputs
    `(("openal" ,openal)))
   (arguments
    `(#:tests? #f)) ; No tests.
   (home-page "https://kcat.strangesoft.net/alure.html")
   (synopsis "Help manage common tasks with OpenAL applications")
   (description "ALURE is a utility library to help manage common tasks with OpenAL applications.  This includes device enumeration and initialization, file loading, and streaming.")
   (license license:x11)))

(define-public springlobby
(package
 (name "springlobby")
 (version "0.270")
 (source (origin
          (method url-fetch)
          (uri "https://springlobby.springrts.com/dl/stable/springlobby-0.270.tar.bz2" )
          
          (patches (list patch-disable-building-bundled-jsoncpp
                    patch-fix-compilation-errors-springlobby))
          (sha256
           (base32
            "1r1g2hw9ipsmsmzbhsi7bxqra1za6x7j1kw12qzl5psqyq8rqbgs"))))
 (build-system cmake-build-system)
 (native-inputs
  `(("gcc" ,gcc-8)
    ("boost" ,boost)
    ("clang" ,clang)
    ("pkg-config" ,pkg-config)))
 (inputs
  `(("wxwidgets" ,(package (inherit wxwidgets)
                           (native-inputs `(,@(package-native-inputs wxwidgets)
                                            ("gcc" ,gcc-8)))))
    ("curl" ,curl)
    ("zlib" ,zlib)
    ("libpng" ,libpng)
    ("libx11" ,libx11)
    ("minizip" ,minizip)
    ("jsoncpp" ,jsoncpp)
    ("libnotify" ,libnotify)
    ("glib" ,glib)
    ("alure" ,alure)
    ("openal" ,openal)
    ("openssl" ,openssl)))
 (arguments
  `(#:tests? #f ; No tests.
    #:configure-flags (list (string-append "-DJsoncpp_INCLUDE_DIR=" (assoc-ref %build-inputs "jsoncpp") "/include"))))
 (home-page "https://springlobby.springrts.com")
 (synopsis "Client for Spring RTS project")
 (description "SpringLobby is a free cross-platform lobby client for the Spring RTS project.")
 (license license:gpl2)))

springlobby
