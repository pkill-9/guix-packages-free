(define-module (pkill9 packages lbreakout2)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages image))

(define-public lbreakout2
  (package
    (name "lbreakout2")
    (version "2.6.5")
    (source
      (origin
        (method url-fetch)
        (uri "http://prdownloads.sourceforge.net/lgames/lbreakout2-2.6.5.tar.gz")
        (sha256
         (base32 "0vwdlyvh7c4y80q5vp7fyfpzbqk9lq3w8pvavi139njkalbxc14i"))
        (snippet
         '(begin
            (use-modules (guix build utils))
            (substitute* "gui/stk.h"
                         (("#include <SDL_mixer.h>") "#include <SDL/SDL_mixer.h>"))))))
    (build-system gnu-build-system)
    (inputs
      `(("sdl" ,sdl)
        ("sdl-mixer" ,sdl-mixer)
        ("sdl-net" ,sdl-net)
        ("libpng" ,libpng)))
    (home-page "http://lgames.sourceforge.net/LBreakout2")
    (synopsis "Breakout game")
    (description "Breakout game. Includes multiplayer mode.")
    (license "license:gpl2")))

lbreakout2
