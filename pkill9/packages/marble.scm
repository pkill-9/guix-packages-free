(define-module (pkill9 packages marble)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages protobuf)
  #:use-module (gnu packages qt))

(define-public marble
  (package
    (name "marble")
    (version "20.12.3")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://invent.kde.org/education/marble.git")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0dl18c782kba92w3mapibpl85sn3wgivaanp722ccjhphfz759z6"))))
    (build-system cmake-build-system)
    (native-inputs `(("protobuf" ,protobuf) ("qttools" ,qttools)))
    (inputs
      `(("qtbase" ,qtbase) ("qtsvg" ,qtsvg) ("qtdeclarative" ,qtdeclarative)))
    (arguments
      `(#:tests? #f)) ; TODO: Fix tests - they do not find the libraries created by marble. Probably can fix with LD_LIBRARY_PATH
    (home-page "https://marble.kde.org")
    (synopsis "Virtual globe and world atlas")
    (description
      "Use Marble similar to a desktop globe; pan around and measure distances.  At closer scale it becomes a world atlas, while OpenStreetMap takes you to street level.  Search for places of interest, view Wikipedia articles, create routes by drag and drop and so much more.")
    (license #f)))

marble
